package com.intib;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.UnsavedRevision;
import com.github.sumimakito.awesomeqr.AwesomeQRCode;
import com.google.protobuf.TextFormat;
import com.intib.core.db.CBManager;
import com.intib.core.db.CouchbaseDB;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.PointInformation;
import com.intib.util.Constant;
import com.valdesekamdem.library.mdtoast.MDToast;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fadhilla Eka Hentino on 11/12/17.
 */

public class Intib {

    private static Constant constant = new Constant();
    private static Retrofit retrofit = null;
    private static Intib _instance;
    ApiManager manager = Intib._getClient().create(ApiManager.class) ;

    public static Intib _getInstance(){
        if(_instance == null){
            _instance = new Intib();
        }
        return _instance;
    }

    public static Retrofit _getClient(){
        if(retrofit != null) {
            retrofit = null;
        }
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(setCredentials())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit _getClient(int gCode){
        if(retrofit != null) {
            retrofit = null;
        }
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(constant.BASE_URL_GMAPS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(setCredentials())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient setCredentials(){
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                        Credentials.basic("intib", "intib123qwe"));

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();
        return okHttpClient;
    }

    public void showError(Context context, String message) {
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.setGravity(Gravity.BOTTOM, 0, 80);
        mdToast.show();
    }

    public void showSuccess(Context context, String message) {
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_SUCCESS);
        mdToast.setGravity(Gravity.BOTTOM, 0, 80);
        mdToast.show();
    }

    public SqlLiteDB getInternalDB(Context context){
        return new SqlLiteDB(context);
    }

    public Object getLocalValue(Context context, String key) {
        try {
            return CBManager._getInstance(context).getDocument(key).getProperty("value");
        } catch (NullPointerException nuexc) {
            return null;
        }
    }

    public void updateLocalValue(Context context, String key, Object value) {
        UnsavedRevision update = CBManager._getInstance(context).getDocument(key).createRevision();
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("value", value);
        update.setProperties(properties);
        try {
            update.save();
        } catch (CouchbaseLiteException ex) {
            Log.e("intib", "CBL operation failed");
        }
    }

    public boolean generateKey(Context context){
        try {
            ECKey _eckey = new ECKey();
            Address _address = _eckey.toAddress(NetworkParameters.prodNet());
            String _transcient_address = _address.toString();
            String _transcient_key = String.valueOf(_eckey.getPrivateKeyAsHex());
            updateLocalValue(context, "private", _transcient_key);
            updateLocalValue(context, "public", _transcient_address);

            AddressInformation information = new AddressInformation();
            information.setAddressKey(_transcient_address);

            Call<ResponseBody> setAddressKey = manager.setAddressKey(information);
            setAddressKey.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception exc) {
            Log.e("intib", exc.toString());
            exc.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean isRegistered(Context context){
        try {
            String _ret = (String) getLocalValue(context, constant.PUBLIC_KEY);
            String _pin = (String) getLocalValue(context, constant.PIN);
            if(_ret == null || _pin == null) {
                return false;
            }else {
                return true;
            }
        } catch (NullPointerException nuexc) {
            return false;
        } catch (Exception exc) {
            return false;
        }
    }

    public boolean checkLogin(Context context,String pin){
        try {
            String _pin = (String) getLocalValue(context, constant.PIN);
            if(_pin.trim().equals(pin.trim())){
                return true;
            }else{
                return false;
            }
        } catch (NullPointerException nuexc) {
            return false;
        } catch (Exception exc) {
            return false;
        }
    }

    public boolean savePin(Context context,String pin){
        try {
            updateLocalValue(context, Constant.PIN, pin);
            return true;
        } catch (Exception exc) {
            Intib._getInstance().showError(context, "Simpan PIN Gagal");
            Log.e("intib", exc.toString());
            exc.printStackTrace();
            return false;
        }
    }

    public String getPublicAddress(Context context) {
        try {
            String _ret = (String) getLocalValue(context, constant.PUBLIC_KEY);
            return _ret;
        }catch(NullPointerException nuexc){
            return null;
        }

    }

    public String getPrivateAddress(Context context) {
        try {
            String _ret = (String) getLocalValue(context, constant.PRIVATE_KEY);
            return _ret;
        }catch(NullPointerException nuexc){
            return null;
        }

    }

    public String getStock(Context context){
        String result = "";
        try {
            String _ret = (String) getLocalValue(context, constant.STOCK);
            if(_ret != null){
                if(_ret.equalsIgnoreCase("0") ){
                    result = "0";
                }else{
                    result = _ret ;
                }
                /*Integer _intRet = Integer.parseInt(_ret);
                if (_intRet > 0) {
                    //result = NumberFormat.getNumberInstance(Locale.GERMAN).format(_intRet);
                    result = ""+_intRet;
                } else {
                    result = "0";
                }*/
            }else{
                result = "0";
            }
        }catch(NullPointerException nuexc){
            result = "0";
        }catch(Exception e){
            result = "0";
        }
        return result;
    }

    public String getDetailStock(Context context){
        String result = "";
        try {
            String _ret = (String) getLocalValue(context, constant.STOCK);
            if(_ret != null){
                if(_ret.equalsIgnoreCase("0") ){
                    result = "0";
                }else{
                    result = _ret ;
                }
                /*Integer _intRet = Integer.parseInt(_ret);
                if (_intRet > 0) {
                    //result = NumberFormat.getNumberInstance(Locale.GERMAN).format(_intRet);
                    result = ""+_intRet;
                } else {
                    result = "0";
                }*/
            }else{
                result = "0";
            }
        }catch(NullPointerException nuexc){
            result = "0";
        }catch(Exception e){
            result = "0";
        }
        return result;
    }

    private boolean generating = false;
    private AlertDialog progressDialog;
    private Bitmap qrBitmap;

    public void getQRAddress(final Activity activity, final ImageView imageView, final int size, String amount,
                             String username, String price) {

        Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.logo_intib4);

        final int margin = 20;
        final float dotScale = 0.3f;
        final int colorDark = Color.BLACK;
        final int colorLight = Color.WHITE;
        final Bitmap background = null;
        final boolean whiteMargin = false;
        final boolean autoColor = true;
        final boolean binarize = false;
        final int binarizeThreshold = 128;
        final boolean roundedDD = false;
        final Bitmap logoImage = bitmap;
        final int logoMargin = 10;
        final int logoCornerRadius = 8;
        final float logoScale = 10;

        String _address = getPublicAddress(activity);
        if (_address == null){
            return;
        }

        String _hex = bytesToHex(String.format("retrive:%s/%s/%s/%s", _address,
                amount,price,username).getBytes());

        if (generating) return;
        generating = true;
        progressDialog = new ProgressDialog.Builder(activity).setMessage("Generating...").setCancelable(false).create();
        progressDialog.show();
        new AwesomeQRCode.Renderer().contents(_hex)
                .size(size).margin(margin).dotScale(dotScale)
                .colorDark(colorDark).colorLight(colorLight)
                .background(background).whiteMargin(whiteMargin)
                .autoColor(autoColor).roundedDots(roundedDD)
                .binarize(binarize).binarizeThreshold(binarizeThreshold)
                .logo(logoImage).logoMargin(logoMargin)
                .logoRadius(logoCornerRadius).logoScale(logoScale)
                .renderAsync(new AwesomeQRCode.Callback() {
                    @Override
                    public void onRendered(AwesomeQRCode.Renderer renderer, final Bitmap bitmap) {
                        qrBitmap = bitmap;
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(qrBitmap);
                                if (progressDialog != null) progressDialog.dismiss();
                                generating = false;
                            }
                        });
                    }

                    @Override
                    public void onError(AwesomeQRCode.Renderer renderer, Exception e) {
                        e.printStackTrace();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressDialog != null) progressDialog.dismiss();
                                generating = false;
                            }
                        });
                    }
                });
    }

    public void getQRAddress(final Activity activity, final ImageView imageView, final String contents, final int size) {

        Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.logo_intib4);

        final int margin = 20;
        final float dotScale = 0.3f;
        final int colorDark = Color.BLACK;
        final int colorLight = Color.WHITE;
        final Bitmap background = null;
        final boolean whiteMargin = false;
        final boolean autoColor = true;
        final boolean binarize = false;
        final int binarizeThreshold = 128;
        final boolean roundedDD = false;
        final Bitmap logoImage = bitmap;
        final int logoMargin = 10;
        final int logoCornerRadius = 8;
        final float logoScale = 10;

        if (generating) return;
        generating = true;
        progressDialog = new ProgressDialog.Builder(activity).setMessage("Generating...").setCancelable(false).create();
        progressDialog.show();
        String publicKey = Intib._getInstance().getPublicAddress(activity);
        String keys = "keys:".concat(contents).concat("/").concat(publicKey);
        String _hex = bytesToHex(String.format("keys:%s/%s", contents,
                publicKey).getBytes());
        new AwesomeQRCode.Renderer().contents(_hex)
                .size(size).margin(margin).dotScale(dotScale)
                .colorDark(colorDark).colorLight(colorLight)
                .background(background).whiteMargin(whiteMargin)
                .autoColor(autoColor).roundedDots(roundedDD)
                .binarize(binarize).binarizeThreshold(binarizeThreshold)
                .logo(logoImage).logoMargin(logoMargin)
                .logoRadius(logoCornerRadius).logoScale(logoScale)
                .renderAsync(new AwesomeQRCode.Callback() {
                    @Override
                    public void onRendered(AwesomeQRCode.Renderer renderer, final Bitmap bitmap) {
                        qrBitmap = bitmap;
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(qrBitmap);
                                if (progressDialog != null) progressDialog.dismiss();
                                generating = false;
                            }
                        });
                    }

                    @Override
                    public void onError(AwesomeQRCode.Renderer renderer, Exception e) {
                        e.printStackTrace();
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (progressDialog != null) progressDialog.dismiss();
                                generating = false;
                            }
                        });
                    }
                });
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public  String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public  byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public String formatAmountWithKilo(String amount){
        String result = amount + " Kg";
        return result;
    }

    public String formatAmountWithKilo(Integer amount){
        String result = String.valueOf(amount) + " Kg";
        return result;
    }

    public String RupiahFormat(double harga) {
        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        return "Rp. " + rupiahFormat.format(harga) + ",00";
    }

    public void addPoint(PointInformation param){
        try {
            Call<ResponseBody> setStock = manager.addPoint(param);
            setStock.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == HttpStatus.SC_OK) {

                    }else{

                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }catch (Exception e){
            Log.e("intib",e.getMessage());
        }
    }

/*  private static AsyncHttpClient client = new AsyncHttpClient();
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }
    public static void callGet() {
        get("", new RequestParams(), new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline

            }
        });
    }
    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }
    public static void callPost() {
        post("", new RequestParams(), new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                // Pull out the first event on the public timeline

            }
        });
    }
    public static void getByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }
    public static void postByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }
    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }*/

}
