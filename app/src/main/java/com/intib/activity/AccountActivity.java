package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.intib.R;
import com.intib.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountActivity extends AppCompatActivity {

    @BindView(R.id.navbar)
    NavigationTabStrip navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnPrivKey)
    ImageButton btnPrivKey;
    @BindView(R.id.btnPubKey)
    ImageButton btnPubKey;
    @BindView(R.id.btnRecovery)
    ImageButton btnRecovery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        navbar.setTitles(R.string.account);
        navbar.setTabIndex(0, true);
        navbar.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.btnPrivKey, R.id.btnPubKey, R.id.btnRecovery})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnPrivKey:
                Intent intent = new Intent(AccountActivity.this, SetPinActivity.class);
                intent.putExtra(Constant.EXTRA_TYPE,Constant.LOGIN_PVKey);
                startActivity(intent);
                break;
            case R.id.btnPubKey:
                Intent intents = new Intent(AccountActivity.this, PublicKeyActivity.class);
                startActivity(intents);
                break;
            case R.id.btnRecovery:
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
