package com.intib.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.adapter.MenuAdapter;
import com.intib.activity.adapter.NavPagerAdapter;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.util.BadgeDrawable;
import com.intib.util.Constant;
import com.intib.util.ToolbarPagerIndicator;

import net.lucode.hackware.magicindicator.MagicIndicator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.HttpStatus;
import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.views.DuoMenuView;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardNavActivity extends AppCompatActivity implements DuoMenuView.OnMenuClickListener  {
    @BindView(R.id.drawer)
    DuoDrawerLayout drawer;

    private static final String[] CHANNELS = new String[]{"DASHBOARD","HISTORI TRANSAKSI"};
    @BindView(R.id.magic_indicator2)
    MagicIndicator magicIndicator2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    private List<String> mDataList = Arrays.asList(CHANNELS);
    private NavPagerAdapter mExamplePagerAdapter;

    private MenuAdapter mMenuAdapter;
    private ViewHolder mViewHolder;

    DuoDrawerToggle duoDrawerToggle;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    private ArrayList<String> mTitles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_nav);
        ButterKnife.bind(this);

        mTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menuOptions)));

        mViewHolder = new ViewHolder();

        handleToolbar();

        handleDrawer();

        handleMenu();

        ToolbarPagerIndicator.initMagicIndicator(this, mDataList, viewPager, magicIndicator2);

        LinearLayout logout = (LinearLayout) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });

        mExamplePagerAdapter = new NavPagerAdapter(getSupportFragmentManager(), mDataList);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                viewPager.setCurrentItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setAdapter(mExamplePagerAdapter);
    }

    @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {
        Intent intent;
        switch (position){
            case 0:
                setDrawerState(false);
                setDrawerState(true);
                break;
            case 1:
                intent = new Intent(DashboardNavActivity.this, ProfileInformationActivity.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(DashboardNavActivity.this, AccountActivity.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(DashboardNavActivity.this, WebIntibActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void setDrawerState(boolean isEnabled) {
        if (isEnabled) {
            mViewHolder.mDuoDrawerLayout.setDrawerLockMode(DuoDrawerLayout.LOCK_MODE_UNLOCKED);
            duoDrawerToggle.setDrawerIndicatorEnabled(true);
            duoDrawerToggle.syncState();

        } else {
            mViewHolder.mDuoDrawerLayout.setDrawerLockMode(DuoDrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            duoDrawerToggle.setDrawerIndicatorEnabled(false);
            duoDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSupportNavigateUp();
                }
            });
            duoDrawerToggle.syncState();
        }
    }

    private class ViewHolder {
        private DuoDrawerLayout mDuoDrawerLayout;
        private DuoMenuView mDuoMenuView;
        private Toolbar mToolbar;

        ViewHolder() {
            mDuoDrawerLayout = (DuoDrawerLayout) findViewById(R.id.drawer);
            mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        }
    }

    private void handleToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(setBadgeCount(this, R.drawable.ic_expense, 3));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void handleDrawer() {
        duoDrawerToggle = new DuoDrawerToggle(this,
                mViewHolder.mDuoDrawerLayout,
                mViewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mViewHolder.mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();
    }

    private void handleMenu() {
        mMenuAdapter = new MenuAdapter(this, mTitles);
        mViewHolder.mDuoMenuView.setOnMenuClickListener(this);
        mViewHolder.mDuoMenuView.setAdapter(mMenuAdapter);
    }
    private Drawable setBadgeCount(Context context, int res, int badgeCount) {
        LayerDrawable icon = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.ic_badge_drawable);
        Drawable mainIcon = ContextCompat.getDrawable(context, res);
        BadgeDrawable badge = new BadgeDrawable(context);
        badge.setCount(String.valueOf(badgeCount));
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
        icon.setDrawableByLayerId(R.id.ic_main_icon, mainIcon);

        return icon;
    }
}
