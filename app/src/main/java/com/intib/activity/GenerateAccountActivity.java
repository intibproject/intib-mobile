package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.PointInformation;
import com.intib.util.Constant;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenerateAccountActivity extends AppCompatActivity {

    @BindView(R.id.layProgress)
    RelativeLayout layProgress;
    @BindView(R.id.tvLogin)
    CustomTextView tvLogin;
    @BindView(R.id.btnSetPin)
    LinearLayout btnSetPin;
    @BindView(R.id.layGenerated)
    RelativeLayout layGenerated;

    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_account);
        ButterKnife.bind(this);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Intib._getInstance().generateKey(getApplicationContext())) {
                    layProgress.setVisibility(View.GONE);
                    layGenerated.setVisibility(View.VISIBLE);
                    btnSetPin.setEnabled(true);

                }
            }
        }, 3000);
    }

    @OnClick(R.id.btnSetPin)
    public void onViewClicked() {
        Intent intent = new Intent(GenerateAccountActivity.this,SetPinActivity.class);
        intent.putExtra(Constant.EXTRA_TYPE, Constant.SET_PIN);
        startActivity(intent);
    }
}
