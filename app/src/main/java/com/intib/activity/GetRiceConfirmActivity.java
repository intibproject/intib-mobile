package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.PointInformation;
import com.intib.model.TrxIdInformation;
import com.intib.model.UsernameInformation;
import com.intib.util.CustomDateFormat;
import com.intib.util.button.CustomButton;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRiceConfirmActivity extends AppCompatActivity {

    @BindView(R.id.tvFrom)
    CustomTextView tvFrom;
    @BindView(R.id.tvDetil)
    CustomTextView tvDetil;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvNominal)
    CustomTextView tvNominal;
    @BindView(R.id.tvPrice)
    CustomTextView tvPrice;
    @BindView(R.id.tvTrxId)
    CustomTextView tvTrxId;
    @BindView(R.id.btnReject)
    CustomButton btnReject;
    @BindView(R.id.btnApprove)
    CustomButton btnApprove;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    @BindView(R.id.tvWallet)
    CustomTextView tvWallet;

    private Integer amount = 0;
    private Integer price = 0;
    private String sender = "";
    private String date = "";
    private String receiver = "";
    private String receiverAddress = "";
    private String trxId = "";

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_rice_confirm);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        setAmount(intent.getIntExtra("amount", 0));
        setPrice(intent.getIntExtra("price", 0));
        setReceiver(intent.getStringExtra("receiver"));
        setReceiverAddress(intent.getStringExtra("receiverAddress"));
        setTrxId(intent.getStringExtra("trxId"));
        setDate(intent.getStringExtra("date"));
        setSender(intent.getStringExtra("sender"));

        tvDate.setText(intent.getStringExtra("date"));
        tvFrom.setText(intent.getStringExtra("sender"));
        tvTrxId.setText(intent.getStringExtra("trxId"));
        tvPrice.setText(String.format("Rp. %,d.-", intent.getIntExtra("price", 0)));
        tvNominal.setText(Intib._getInstance().formatAmountWithKilo(intent.getIntExtra("amount", 0)));
        tvDetil.setText("Terima Beras");
        tvWallet.setText(Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(this)));
    }

    @OnClick({R.id.btnReject, R.id.btnApprove})
    public void onViewClicked(View view) {
        TrxIdInformation param = new TrxIdInformation();
        param.setTransactionId(BigInteger.valueOf(Long.parseLong(getTrxId())));
        switch (view.getId()) {
            case R.id.btnReject:
                Call<ResponseBody> rejectTransaction = manager.rejectTransaction(param);
                rejectTransaction.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.code() == HttpStatus.SC_OK) {
                                Intib._getInstance().showSuccess(GetRiceConfirmActivity.this, "Data Berhasil di Tolak");
                                Intent intent = new Intent(GetRiceConfirmActivity.this, GetRiceNavActivity.class);
                                startActivity(intent);
                                Intent receiptIntent = new Intent(GetRiceConfirmActivity.this, ReceiptGetRiceActivity.class);
                                receiptIntent.putExtra("date",
                                        CustomDateFormat._getInstance().getDateTime(new Date().getTime()));
                                receiptIntent.putExtra("sender",
                                        getSender());
                                receiptIntent.putExtra("trxId",
                                        getTrxId());
                                receiptIntent.putExtra("amount",
                                        getAmount());
                                receiptIntent.putExtra("price",
                                        getPrice());
                                receiptIntent.putExtra("status",
                                        "DITOLAK");
                                startActivity(receiptIntent);
                            } else {
                                Intib._getInstance().showError(GetRiceConfirmActivity.this, "Transaksi Gagal");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
                break;
            case R.id.btnApprove:
                Call<ResponseBody> approvedTransaction = manager.approvedTransaction(param);
                approvedTransaction.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.code() == HttpStatus.SC_OK) {
                                PointInformation param = new PointInformation();
                                param.setAddressKey(Intib._getInstance().getPublicAddress(GetRiceConfirmActivity.this));
                                param.setPoint(50);
                                Intib._getInstance().addPoint(param);

                                UsernameInformation paramUser = new UsernameInformation();
                                paramUser.setUsername(getSender().trim());

                                Call<ResponseBody> checkAccountByUsername = manager.checkAccountByUsername(paramUser);
                                checkAccountByUsername.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        try {
                                            if (response.code() == HttpStatus.SC_OK) {
                                                String jsonData = response.body().string();
                                                if (!jsonData.equalsIgnoreCase("")) {
                                                    JSONObject jsonObj = new JSONObject(jsonData);
                                                    String receiverAddress = jsonObj.getString("addressKey");
                                                    PointInformation param = new PointInformation();
                                                    param.setAddressKey(receiverAddress);
                                                    param.setPoint(50);
                                                    Intib._getInstance().addPoint(param);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });

                                Intent intent = new Intent(GetRiceConfirmActivity.this, GetRiceNavActivity.class);
                                startActivity(intent);
                                Intent receiptIntent = new Intent(GetRiceConfirmActivity.this, ReceiptGetRiceActivity.class);
                                receiptIntent.putExtra("date",
                                        CustomDateFormat._getInstance().getDateTime(new Date().getTime()));
                                receiptIntent.putExtra("sender",
                                        getSender());
                                receiptIntent.putExtra("trxId",
                                        getTrxId());
                                receiptIntent.putExtra("amount",
                                        getAmount());
                                receiptIntent.putExtra("price",
                                        getPrice());
                                receiptIntent.putExtra("status",
                                        "DISETUJUI");
                                startActivity(receiptIntent);
                                Intib._getInstance().showSuccess(GetRiceConfirmActivity.this, "Data Berhasil di Setujui");
                            } else {
                                Intib._getInstance().showError(GetRiceConfirmActivity.this, "Transaksi Gagal");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
                break;
        }
    }
}
