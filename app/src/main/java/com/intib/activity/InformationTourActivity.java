package com.intib.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.fragment.InformationTourFragment;
import com.intib.util.Constant;
import com.intib.util.ViewPagerCustomDuration;
import com.intib.util.fonts.CustomTextView;
import com.nineoldandroids.view.ViewHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InformationTourActivity extends AppCompatActivity {

    static final int NUM_PAGES = 4;

    PagerAdapter pagerAdapter;
    boolean isOpaque = true;
    @BindView(R.id.pager)
    ViewPagerCustomDuration pager;
    @BindView(R.id.circles)
    LinearLayout circles;
    @BindView(R.id.btnRecovery)
    LinearLayout btnRecovery;
    @BindView(R.id.btnRegistration)
    LinearLayout btnRegistration;
    @BindView(R.id.tvRecovery)
    CustomTextView tvRecovery;
    @BindView(R.id.tvRegistration)
    CustomTextView tvRegistration;
    @BindView(R.id.btnGroup)
    LinearLayout btnGroup;
    @BindView(R.id.tvLogin)
    CustomTextView tvLogin;
    @BindView(R.id.btnLogin)
    LinearLayout btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_information_tour);
        ButterKnife.bind(this);

        pager.startAutoScroll();
        pager.setInterval(3000);
        pager.setCycle(true);
        pager.setStopScrollWhenTouch(true);

        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new CrossfadePageTransformer());
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == NUM_PAGES - 2 && positionOffset > 0) {
                    if (isOpaque) {
                        pager.setBackgroundColor(Color.TRANSPARENT);
                        isOpaque = false;
                    }
                } else {
                    if (!isOpaque) {
                        pager.setBackgroundColor(getResources().getColor(R.color.primary_material_light));
                        isOpaque = true;
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        buildCircles();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Intib._getInstance().isRegistered(this)) {
            btnGroup.setVisibility(View.INVISIBLE);
            btnLogin.setVisibility(View.VISIBLE);
        } else {
            btnGroup.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pager != null) {
            pager.clearOnPageChangeListeners();
        }
    }

    @OnClick({R.id.btnRegistration,R.id.btnLogin,R.id.btnRecovery})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnRegistration:
                intent = new Intent(InformationTourActivity.this, GenerateAccountActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLogin:
                intent = new Intent(InformationTourActivity.this, SetPinActivity.class);
                intent.putExtra(Constant.EXTRA_TYPE, Constant.LOGIN_PIN);
                startActivity(intent);
                break;
            case R.id.btnRecovery:
                intent = new Intent(InformationTourActivity.this, RecoveryAccountActivity.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            InformationTourFragment tp = null;
            switch (position) {
                case 0:
                    tp = InformationTourFragment.newInstance(R.layout.information_tour1);
                    break;
                case 1:
                    tp = InformationTourFragment.newInstance(R.layout.information_tour2);
                    break;
                case 2:
                    tp = InformationTourFragment.newInstance(R.layout.information_tour1);
                    break;
                case 3:
                    tp = InformationTourFragment.newInstance(R.layout.information_tour2);
                    break;
            }

            return tp;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class CrossfadePageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View page, float position) {
            int pageWidth = page.getWidth();

            View pageLayout = page.findViewById(R.id.pageLayout);
            View backgroundView = page.findViewById(R.id.welcome_fragment);
            View text_head = page.findViewById(R.id.heading);
            View welcomeImage01 = page.findViewById(R.id.welcome_01);
            View welcomeImage02 = page.findViewById(R.id.welcome_02);
            View welcomeImage03 = page.findViewById(R.id.welcome_01);
            View welcomeImage04 = page.findViewById(R.id.welcome_02);

            if (0 <= position && position < 1) {
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }
            if (-1 < position && position < 0) {
                ViewHelper.setTranslationX(page, pageWidth * -position);
            }

            if (position <= -1.0f || position >= 1.0f) {
            } else if (position == 0.0f) {
            } else {

                if (pageLayout != null) {
                    ViewHelper.setTranslationX(pageLayout, pageWidth * position);
                    ViewHelper.setAlpha(pageLayout, 1.0f - Math.abs(position));
                }

                if (backgroundView != null) {
                    ViewHelper.setAlpha(backgroundView, 1.0f - Math.abs(position));
                }


                if (text_head != null) {
                    ViewHelper.setTranslationX(text_head, pageWidth * position);
                    ViewHelper.setAlpha(text_head, 1.0f - Math.abs(position));
                }

                if (welcomeImage01 != null) {
                    ViewHelper.setTranslationX(welcomeImage01, (float) (pageWidth / 2 * position));
                    ViewHelper.setAlpha(welcomeImage01, 1.0f - Math.abs(position));
                }

                if (welcomeImage02 != null) {
                    ViewHelper.setTranslationX(welcomeImage02, (float) (pageWidth / 2 * position));
                    ViewHelper.setAlpha(welcomeImage02, 1.0f - Math.abs(position));
                }

                if (welcomeImage03 != null) {
                    ViewHelper.setTranslationX(welcomeImage03, (float) (pageWidth / 2 * position));
                    ViewHelper.setAlpha(welcomeImage03, 1.0f - Math.abs(position));
                }

                if (welcomeImage04 != null) {
                    ViewHelper.setTranslationX(welcomeImage04, (float) (pageWidth / 2 * position));
                    ViewHelper.setAlpha(welcomeImage04, 1.0f - Math.abs(position));
                }
            }
        }
    }

    private void buildCircles() {
        circles = LinearLayout.class.cast(findViewById(R.id.circles));

        float scale = getResources().getDisplayMetrics().density;
        int padding = (int) (5 * scale + 0.5f);

        for (int i = 0; i < NUM_PAGES; i++) {
            ImageView circle = new ImageView(this);
            circle.setImageResource(R.drawable.circle_normal);
            circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            circle.setAdjustViewBounds(true);
            circle.setPadding(padding, 0, padding, 0);
            circles.addView(circle);
        }

        setIndicator(0);
    }

    private void setIndicator(int index) {
        if (index < NUM_PAGES) {
            for (int i = 0; i < NUM_PAGES; i++) {
                ImageView circle = (ImageView) circles.getChildAt(i);
                if (i == index) {
                    circle.setColorFilter(getResources().getColor(R.color.dark_grey_color));
                } else {
                    circle.setColorFilter(getResources().getColor(android.R.color.transparent));
                }
            }
        }
    }
}
