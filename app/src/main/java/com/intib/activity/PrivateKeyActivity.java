package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.intib.Intib;
import com.intib.R;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrivateKeyActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivQr)
    ImageView ivQr;
    @BindView(R.id.etBarcode)
    CustomTextView etBarcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_key);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kunci Rahasia");
        String _address = Intib._getInstance().getPrivateAddress(this);
        Intib._getInstance().getQRAddress(this, ivQr, _address, 800);
        etBarcode.setText(_address);
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(PrivateKeyActivity.this,AccountActivity.class);
        startActivity(returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent returnIntent = new Intent(PrivateKeyActivity.this,AccountActivity.class);
                startActivity(returnIntent);
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
