package com.intib.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.PendingTransactionDetailInformation;
import com.intib.model.PendingTransactionInformation;
import com.intib.model.UserInformation;
import com.intib.model.UsernameInformation;
import com.intib.util.Constant;
import com.intib.util.button.CustomButton;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileInformationActivity extends AppCompatActivity {

    @BindView(R.id.navbar)
    NavigationTabStrip navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.kyc_username)
    CustomEditText kycUsername;
    @BindView(R.id.kyc_username_header)
    CustomTextView kycUsernameHeader;
    @BindView(R.id.kyc_nama)
    CustomEditText kycNama;
    @BindView(R.id.kyc_nama_header)
    CustomTextView kycNamaHeader;
    @BindView(R.id.kyc_phone)
    CustomEditText kycPhone;
    @BindView(R.id.kyc_phone_header)
    CustomTextView kycPhoneHeader;
    @BindView(R.id.btnClearInput)
    CustomButton btnClearInput;
    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;
    @BindView(R.id.layFooterButton)
    LinearLayout layFooterButton;
    @BindView(R.id.btnLoc)
    CustomButton btnLoc;

    int PLACE_PICKER_REQUEST = 1;
    @BindView(R.id.evAddress)
    CustomEditText evAddress;
    @BindView(R.id.evCity)
    CustomEditText evCity;
    @BindView(R.id.evProvince)
    CustomEditText evProvince;
    @BindView(R.id.rlAddress)
    RelativeLayout rlAddress;
    @BindView(R.id.rlCity)
    RelativeLayout rlCity;
    @BindView(R.id.rlProvince)
    RelativeLayout rlProvince;

    ApiManager gmManager = Intib._getClient(1).create(ApiManager.class);
    ApiManager manager = Intib._getClient().create(ApiManager.class);
    private boolean result = false;
    private String usernameOld = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_information);
        ButterKnife.bind(this);
        navbar.setTitles(R.string.profile);
        navbar.setTabIndex(0, true);
        navbar.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String jsonStr = Intib._getInstance().getLocalValue(this, Constant.PROFILE) != null ?
                Intib._getInstance().getLocalValue(this, Constant.PROFILE).toString() : "";

        if (!jsonStr.equalsIgnoreCase("")) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(jsonStr);
                rlAddress.setVisibility(View.VISIBLE);
                evAddress.setText(obj.getString("fulladdress"));
                kycUsername.setText(obj.getString("username"));
                kycNama.setText(obj.getString("name"));
                kycPhone.setText(obj.getString("phone"));
                usernameOld=obj.getString("username");
                /*rlCity.setVisibility(View.VISIBLE);
                evCity.setText(obj.getString("city"));
                rlProvince.setVisibility(View.VISIBLE);
                evProvince.setText(obj.getString("province"));*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            rlAddress.setVisibility(View.GONE);
            rlCity.setVisibility(View.GONE);
            rlProvince.setVisibility(View.GONE);
            /*new SweetAlertDialog(ProfileInformationActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Isi Identitas Diri")
                    .setContentText("Dengan Isi Identitasi Diri, Anda dapat menggunakan Aplikasi Intib :)")
                    .setConfirmText("Isi")
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {

                        }
                    })
                    .show();*/
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent intent = new Intent(ProfileInformationActivity.this,DashboardNavActivity.class);
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.btnLoc, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLoc:
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST); // for activty
                    //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST); // for fragment
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btnSubmit:
                if(validation(this)) {
                    String jsonStr = Intib._getInstance().getLocalValue(this, Constant.LOCATION) != null ?
                            Intib._getInstance().getLocalValue(this, Constant.LOCATION).toString() : "";
                    JSONObject obj = null;
                    try {
                        String username = kycUsername.getText().toString();
                        String name = kycNama.getText().toString();
                        String phone = kycPhone.getText().toString();

                        obj = new JSONObject(jsonStr);
                        obj.put("username", username.trim());
                        obj.put("name", name.trim());
                        obj.put("phone", phone.trim());
                        Intib._getInstance().updateLocalValue(this, Constant.PROFILE, obj.toString());

                        UserInformation param = new UserInformation();
                        param.setAddressKey(Intib._getInstance().getPublicAddress(this));
                        param.setUsername(username.trim());
                        param.setName(name.trim());
                        param.setPhone(phone.trim());
                        param.setAddress(obj.getString("fulladdress"));
                        param.setKabupaten(obj.getString("city"));
                        param.setProvinsi(obj.getString("province"));
                        param.setLat(obj.getDouble("lat"));
                        param.setLon(obj.getDouble("long"));

                        Call<ResponseBody> pendingTransaction = manager.updateProfile(param);
                        pendingTransaction.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if(response.body() != null) {
                                    Intent intent = new Intent(ProfileInformationActivity.this,DashboardNavActivity.class);
                                    startActivity(intent);
                                    Intib._getInstance().showSuccess(ProfileInformationActivity.this, "Sukses Melakukan Perubahan Identitas Diri");
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                System.out.println(t.getMessage());
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private boolean validation(Context context){
        String username = kycUsername.getText().toString() == "" ? "" : kycUsername.getText().toString();
        String name = kycNama.getText().toString() == "" ? "" : kycNama.getText().toString();
        String phone = kycPhone.getText().toString() == "" ? "" : kycPhone.getText().toString();
        String address = evAddress.getText().toString() == "" ? "" : evAddress.getText().toString();
        String city = evCity.getText().toString() == "" ? "" : evCity.getText().toString();

        if(username.equalsIgnoreCase("")){
            result = false;
            Intib._getInstance().showError(context,"Username Harus Diisi");
        }else{
            if(username.length()<6 && username.length() > 20){
                result = false;
                Intib._getInstance().showError(context,"Username Harus 6-20 Karakter");
            }else{
                if(name.equalsIgnoreCase("")){
                    result = false;
                    Intib._getInstance().showError(context,"Nama Harus Diisi");
                }else{
                    if(phone.equalsIgnoreCase("")){
                        result = false;
                        Intib._getInstance().showError(context,"Nomor HP Harus Diisi");
                    }else {
                        if (!phone.toLowerCase().matches("[0-9]{8,}")) {
                            result = false;
                            Intib._getInstance().showError(context, "Format Nomor HP Salah");
                        }else{
                            if(address.equalsIgnoreCase("") && city.equalsIgnoreCase("")){
                                result = false;
                                Intib._getInstance().showError(context, "Harap Mengatur Lokasi Rumah / Toko / Pabrik Anda");
                            }else{
                                if(!usernameOld.trim().equals(username.trim())){
                                    final boolean resp;
                                    UsernameInformation param = new UsernameInformation();
                                    param.setUsername(username);
                                    Call<ResponseBody> checkAccountByUsername = manager.checkAccountByUsername(param);
                                    checkAccountByUsername.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            try {
                                                if (response.code() == HttpStatus.SC_OK) {
                                                    result = false;
                                                    Intib._getInstance().showError(ProfileInformationActivity.this,"Username Sudah Terdaftar");
                                                } else {
                                                    result = true;
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                result = false;
                                                Intib._getInstance().showError(ProfileInformationActivity.this,"Gagal Menyimpan Identitas Diri");
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                            result = false;
                                            Intib._getInstance().showError(ProfileInformationActivity.this,"Gagal Menyimpan Identitas Diri");
                                        }
                                    });
                                }else{
                                    result = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                try {
                    /*Geocoder geocoder = new Geocoder(this);
                    List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getSubAdminArea();
                    String province = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();*/

                    //json?latlng=-6.241212,106.628172
                    final double lon = place.getLatLng().longitude;
                    final double lat = place.getLatLng().latitude;
                    String longlat = String.format("%f,%f",lat,lon);

                    Call<ResponseBody> pendingTransaction = gmManager.getLoc(longlat);
                    pendingTransaction.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if(response.body() != null) {
                                String jsonData = null;
                                try {
                                    jsonData = response.body().string();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    JSONObject obj = new JSONObject(jsonData);
                                    JSONArray addrComp = ((JSONArray)obj.get("results")).getJSONObject(0).getJSONArray("address_components");

                                    String address = "";
                                    String city = "";
                                    String province = "";
                                    String fullAddress = "";
                                    for(int x = 0; x < addrComp.length();x++ ){
                                        String label = "";
                                        label = ((JSONArray) ((JSONObject) addrComp.get(x)).get("types")).get(0).toString();

                                        if(label.equalsIgnoreCase("route")){
                                            address = ((JSONObject) addrComp.get(x)).getString("long_name");
                                        }
                                        if(label.equalsIgnoreCase("administrative_area_level_2")){
                                            city = ((JSONObject) addrComp.get(x)).getString("long_name");
                                        }
                                        if(label.equalsIgnoreCase("administrative_area_level_1")){
                                            province = ((JSONObject) addrComp.get(x)).getString("long_name");
                                        }
                                    }
                                    fullAddress = address.concat(", ").concat(city).concat(", ").concat(province);


                                    JSONObject objs = new JSONObject();
                                    objs.put("address", address);
                                    objs.put("city", city);
                                    objs.put("province", province);
                                    objs.put("fulladdress", fullAddress);
                                    objs.put("long", lon);
                                    objs.put("lat", lat);

                                    Intib._getInstance().updateLocalValue(ProfileInformationActivity.this, Constant.LOCATION, objs.toString());
                                    rlAddress.setVisibility(View.VISIBLE);
                                    evAddress.setText(fullAddress);
                                    /*rlCity.setVisibility(View.VISIBLE);
                                    evCity.setText(city);
                                    rlProvince.setVisibility(View.VISIBLE);
                                    evProvince.setText(province);*/
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            System.out.println(t.getMessage());
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(place.getAddress());
                System.out.println(place.getLatLng().latitude);
            }
        }
    }
}
