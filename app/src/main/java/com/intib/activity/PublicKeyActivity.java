package com.intib.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PublicKeyActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivQr)
    ImageView ivQr;
    @BindView(R.id.etBarcode)
    CustomTextView etBarcode;
    @BindView(R.id.tvCopyVA)
    CustomTextView tvCopyVA;
    @BindView(R.id.layCopyVA)
    LinearLayout layCopyVA;

    String stbarcode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_key);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kunci Publik");

        String _address = Intib._getInstance().getPublicAddress(this);
        Intib._getInstance().getQRAddress(this, ivQr, _address, 1000);
        etBarcode.setText(_address);
        stbarcode = _address;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.layCopyVA})
    public void onViewClicked(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Public Key", stbarcode);
        clipboard.setPrimaryClip(clip);
        Intib._getInstance().showSuccess(this, "Kunci Publik telah di salin");
    }
}
