package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.intib.Intib;
import com.intib.R;
import com.intib.util.button.CustomButton;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceiptBuyActivity extends AppCompatActivity {

    @BindView(R.id.navbar)
    NavigationTabStrip navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.layHeader)
    LinearLayout layHeader;
    @BindView(R.id.tvStatus)
    CustomTextView tvStatus;
    @BindView(R.id.tableRow21)
    TableRow tableRow21;
    @BindView(R.id.tvSender)
    CustomTextView tvSender;
    @BindView(R.id.tableRow1)
    TableRow tableRow1;
    @BindView(R.id.tvReceiver)
    CustomTextView tvReceiver;
    @BindView(R.id.tvDariReceipt)
    CustomTextView tvDariReceipt;
    @BindView(R.id.tableRow2)
    TableRow tableRow2;
    @BindView(R.id.tvDateReceipt)
    CustomTextView tvDateReceipt;
    @BindView(R.id.tableRow3)
    TableRow tableRow3;
    @BindView(R.id.tvTime)
    CustomTextView tvTime;
    @BindView(R.id.tableRow31)
    TableRow tableRow31;
    @BindView(R.id.tvAmount)
    CustomTextView tvAmount;
    @BindView(R.id.tableRow32)
    TableRow tableRow32;
    @BindView(R.id.tvPrice)
    CustomTextView tvPrice;
    @BindView(R.id.tableRow36)
    TableRow tableRow36;
    @BindView(R.id.tvTotal)
    CustomTextView tvTotal;
    @BindView(R.id.tableRow33)
    TableRow tableRow33;
    @BindView(R.id.tabla_cuerpo)
    TableLayout tablaCuerpo;
    @BindView(R.id.layContent)
    RelativeLayout layContent;
    @BindView(R.id.sparatorBtm)
    View sparatorBtm;
    @BindView(R.id.tvTransactionId)
    CustomTextView tvTransactionId;
    @BindView(R.id.sparator)
    View sparator;
    @BindView(R.id.btnBack)
    CustomButton btnBack;
    @BindView(R.id.btnApprove)
    CustomButton btnApprove;
    @BindView(R.id.btnSell)
    CustomButton btnSell;
    @BindView(R.id.btnCancel)
    CustomButton btnCancel;
    @BindView(R.id.layFooterButton)
    LinearLayout layFooterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_buy);
        ButterKnife.bind(this);

        navbar.setTitles(R.string.receipt);
        navbar.setTabIndex(0, true);
        navbar.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        if (!String.valueOf(intent.getStringExtra("sender")).isEmpty()) {
            tvSender.setText(String.valueOf(intent.getStringExtra("sender")));
        }
        if (!String.valueOf(intent.getStringExtra("phone")).isEmpty()) {
            tvReceiver.setText(String.valueOf(intent.getStringExtra("phone")));
        }
        if (!String.valueOf(intent.getStringExtra("date")).isEmpty()) {
            tvDateReceipt.setText(String.valueOf(intent.getStringExtra("date")));
        }
        if (!String.valueOf(intent.getStringExtra("trxId")).isEmpty()) {
            tvTransactionId.setText(String.valueOf(intent.getStringExtra("trxId")));
        }
        if (!String.valueOf(intent.getIntExtra("amount", 0)).isEmpty()) {
            tvAmount.setText(String.valueOf(Intib._getInstance().formatAmountWithKilo(intent.getIntExtra("amount", 0))));
        }
        if (!String.valueOf(intent.getIntExtra("price", 0)).isEmpty()) {
            tvPrice.setText(String.valueOf(Intib._getInstance().RupiahFormat(intent.getIntExtra("price", 0))));
        }
        if (!String.valueOf(intent.getIntExtra("total", 0)).isEmpty()) {
            tvTotal.setText(String.valueOf(Intib._getInstance().RupiahFormat(intent.getIntExtra("total", 0))));
        }
    }

    @OnClick(R.id.btnBack)
    public void onViewClicked() {
        Intent intent = new Intent(this, DashboardNavActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
