package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.intib.Intib;
import com.intib.R;
import com.intib.util.button.CustomButton;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceiptGetRiceActivity extends AppCompatActivity {


    @BindView(R.id.navbar)
    NavigationTabStrip navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.layHeader)
    LinearLayout layHeader;
    @BindView(R.id.tvDetail)
    CustomTextView tvDetail;
    @BindView(R.id.tvDetil)
    CustomTextView tvDetil;
    @BindView(R.id.tableRow1)
    TableRow tableRow1;
    @BindView(R.id.tvStatus)
    CustomTextView tvStatus;
    @BindView(R.id.tableRow21)
    TableRow tableRow21;
    @BindView(R.id.tvDariReceipt)
    CustomTextView tvDariReceipt;
    @BindView(R.id.tableRow2)
    TableRow tableRow2;
    @BindView(R.id.tvDateReceipt)
    CustomTextView tvDateReceipt;
    @BindView(R.id.tableRow3)
    TableRow tableRow3;
    @BindView(R.id.tvTime)
    CustomTextView tvTime;
    @BindView(R.id.tableRow31)
    TableRow tableRow31;
    @BindView(R.id.tvAmount)
    CustomTextView tvAmount;
    @BindView(R.id.tableRow32)
    TableRow tableRow32;
    @BindView(R.id.tvTotal)
    CustomTextView tvTotal;
    @BindView(R.id.tableRow33)
    TableRow tableRow33;
    @BindView(R.id.tabla_cuerpo)
    TableLayout tablaCuerpo;
    @BindView(R.id.layContent)
    RelativeLayout layContent;
    @BindView(R.id.sparatorBtm)
    View sparatorBtm;
    @BindView(R.id.tvTransactionId)
    CustomTextView tvTransactionId;
    @BindView(R.id.sparator)
    View sparator;
    @BindView(R.id.btnReject)
    CustomButton btnReject;
    @BindView(R.id.btnBack)
    CustomButton btnBack;
    @BindView(R.id.layFooterButton)
    LinearLayout layFooterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_get_rice);
        ButterKnife.bind(this);

        navbar.setTitles(R.string.receipt);
        navbar.setTabIndex(0, true);
        navbar.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String username = Intib._getInstance().getLocalValue(this, "username") == null ? "" : Intib._getInstance().getLocalValue(this, "username").toString();
        tvDariReceipt.setText(username);
        tvDetil.setText("Terima Beras");
        if (!String.valueOf(intent.getStringExtra("status")).isEmpty()) {
            tvStatus.setText(String.valueOf(intent.getStringExtra("status")).toUpperCase());
        }
        if (!String.valueOf(intent.getStringExtra("sender")).isEmpty()) {
            tvDariReceipt.setText(String.valueOf(intent.getStringExtra("sender")));
        }
        if (!String.valueOf(intent.getStringExtra("date")).isEmpty()) {
            tvDateReceipt.setText(String.valueOf(intent.getStringExtra("date")));
        }
        if (!String.valueOf(intent.getStringExtra("time")).isEmpty()) {
            tvTime.setText(String.valueOf(intent.getStringExtra("time")));
        }
        if (!String.valueOf(intent.getStringExtra("trxId")).isEmpty()) {
            tvTransactionId.setText(String.valueOf(intent.getStringExtra("trxId")));
        }
        if (!String.valueOf(intent.getIntExtra("amount", 0)).isEmpty()) {
            tvAmount.setText(Intib._getInstance().formatAmountWithKilo(intent.getIntExtra("amount", 0)));
        }
        if (!String.valueOf(intent.getIntExtra("price", 0)).isEmpty()) {
            tvTotal.setText(Intib._getInstance().RupiahFormat(Double.valueOf(String.valueOf(intent.getIntExtra("price", 0)))));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnBack)
    public void onViewClicked() {
        Intent intent = new Intent(ReceiptGetRiceActivity.this,DashboardNavActivity.class);
        startActivity(intent);
    }
}
