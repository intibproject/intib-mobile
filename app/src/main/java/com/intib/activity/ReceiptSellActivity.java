package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableRow;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.intib.Intib;
import com.intib.R;
import com.intib.activity.fragment.SellListFragment;
import com.intib.manager.ApiManager;
import com.intib.model.RemoveSellInformation;
import com.intib.util.button.CustomButton;
import com.intib.util.fonts.CustomTextView;

import java.math.BigInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptSellActivity extends AppCompatActivity {

    @BindView(R.id.tvDetail)
    CustomTextView tvDetail;
    @BindView(R.id.tvDetil)
    CustomTextView tvDetil;
    @BindView(R.id.tvStatus)
    CustomTextView tvStatus;
    @BindView(R.id.tvDariReceipt)
    CustomTextView tvDariReceipt;
    @BindView(R.id.tvDateReceipt)
    CustomTextView tvDateReceipt;
    @BindView(R.id.tvTime)
    CustomTextView tvTime;
    @BindView(R.id.tvAmount)
    CustomTextView tvAmount;
    @BindView(R.id.tvTotal)
    CustomTextView tvTotal;
    @BindView(R.id.tvTransactionId)
    CustomTextView tvTransactionId;
    @BindView(R.id.btnReject)
    CustomButton btnReject;
    @BindView(R.id.btnApprove)
    CustomButton btnApprove;
    @BindView(R.id.btnSell)
    CustomButton btnSell;
    @BindView(R.id.btnCancel)
    CustomButton btnCancel;
    @BindView(R.id.navbar)
    NavigationTabStrip navbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tableRow2)
    TableRow tableRow2;
    @BindView(R.id.tvPrice)
    CustomTextView tvPrice;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    private String marketId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_sell);
        ButterKnife.bind(this);

        navbar.setTitles(R.string.receipt);
        navbar.setTabIndex(0, true);
        navbar.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        tvDetil.setVisibility(View.GONE);
        tvDetail.setVisibility(View.GONE);
        if (!String.valueOf(intent.getStringExtra("status")).isEmpty()) {
            String status = String.valueOf(intent.getStringExtra("status")).toUpperCase();
            formatView(status);
            tvStatus.setText(status);
        }else{
            tvStatus.setVisibility(View.GONE);
        }
        if (!String.valueOf(intent.getStringExtra("sender")).isEmpty()) {
            tvDariReceipt.setText(String.valueOf(intent.getStringExtra("sender")));
        }
        if (!String.valueOf(intent.getStringExtra("date")).isEmpty()) {
            tvDateReceipt.setText(String.valueOf(intent.getStringExtra("date")));
        }
        if (!String.valueOf(intent.getStringExtra("time")).isEmpty()) {
            tvTime.setText(String.valueOf(intent.getStringExtra("time")));
        }
        if (!String.valueOf(intent.getStringExtra("sender")).isEmpty()) {
            tvDariReceipt.setText(String.valueOf(intent.getStringExtra("sender")));
        }
        if (!String.valueOf(intent.getStringExtra("trxId")).isEmpty()) {
            tvTransactionId.setText(String.valueOf(intent.getStringExtra("trxId")));
            marketId=String.valueOf(intent.getStringExtra("trxId"));
        }
        if (!String.valueOf(intent.getStringExtra("amount")).isEmpty()) {
            tvAmount.setText(String.valueOf(intent.getStringExtra("amount")));
        }
        if (!String.valueOf(intent.getStringExtra("price")).isEmpty()) {
            tvPrice.setText(String.valueOf(intent.getStringExtra("price")));
        }
        if (!String.valueOf(intent.getStringExtra("total")).isEmpty()) {
            tvTotal.setText(String.valueOf(intent.getStringExtra("total")));
        }
    }

    private void formatView(String status) {
        if (status.equalsIgnoreCase("JUAL")) {
            btnApprove.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);
            btnSell.setVisibility(View.GONE);
            btnCancel.setVisibility(View.VISIBLE);
            tableRow2.setVisibility(View.GONE);
        } else if (status.equalsIgnoreCase("KONFIRMASI")) {
            btnApprove.setVisibility(View.VISIBLE);
            btnReject.setVisibility(View.VISIBLE);
            tableRow2.setVisibility(View.VISIBLE);
            btnSell.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        } else {
            btnApprove.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);
            btnSell.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.GONE);
            tableRow2.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.btnReject, R.id.btnApprove, R.id.btnSell, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnReject:
                break;
            case R.id.btnApprove:
                break;
            case R.id.btnSell:
                break;
            case R.id.btnCancel:
                try {
                    RemoveSellInformation param = new RemoveSellInformation();
                    param.setMarketId(new BigInteger(marketId));
                    Call<ResponseBody> setStock = manager.removeProduct(param);
                    setStock.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == HttpStatus.SC_OK) {
                                Intent intent = new Intent(ReceiptSellActivity.this, SellNavActivity.class);
                                startActivity(intent);
                                Intib._getInstance().showSuccess(ReceiptSellActivity.this, "Data Penjualan Di Cancel");
                            }else{
                                Intib._getInstance().showError(ReceiptSellActivity.this, "Gagal Cancel Data Penjualan");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Intib._getInstance().showError(ReceiptSellActivity.this, "Gagal Cancel Data Penjualan");
                        }
                    });
                }catch (Exception e){
                    Log.e("intib",e.getMessage());
                }
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
