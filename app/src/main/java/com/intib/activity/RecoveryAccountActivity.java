package com.intib.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.UserInformation;
import com.intib.util.Constant;
import com.intib.util.SweetDialog;
import com.intib.util.qrcode.ZXingScannerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;

public class RecoveryAccountActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    @BindView(R.id.layWallet)
    RelativeLayout layWallet;
    @BindView(R.id.layHeaderWallet)
    RelativeLayout layHeaderWallet;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;

    private ZXingScannerView mScannerView;
    private static final int REQUEST_CAMERA = 1;
    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_account);

        List<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);

        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(this);
        mScannerView.setLaserEnabled(false);
        mScannerView.setIsBorderCornerRounded(true);
        mScannerView.setSquareViewFinder(true);
        mScannerView.setBorderLineLength(200);
        mScannerView.setBorderCornerRadius(20);
        mScannerView.setFormats(formats);
        contentFrame.addView(mScannerView);
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {

            } else {
                requestPermission();
            }
        }
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(this, CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {

                    } else {
                        Intib._getInstance().showError(this, "Permission Denied, You cannot access and camera");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(Result rawResult) {
        final Context _this = this;

        String _data = new String(Intib._getInstance().hexStringToByteArray(rawResult.toString()));
        final String address;
        final String[] datas;
        final String privateKey;
        final String publicKey;
        try {
            datas = _data.split("key:")[1].split("/");
            privateKey = datas[1];
            publicKey = datas[2];
        } catch (Exception exc) {
            mScannerView.startCamera();
            Intib._getInstance().showError(_this, "QRCode tidak dikenali");
            finish();
            return;
        }
        if (_data.startsWith("key:")) {
            int saldo = Integer.parseInt(Intib._getInstance().getStock(this));
            SweetDialog.ProgressDialog((Activity) _this);
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            SweetDialog.Dismiss();
                            Intib._getInstance()._getInstance().updateLocalValue(RecoveryAccountActivity.this, "private", privateKey);
                            Intib._getInstance()._getInstance().updateLocalValue(RecoveryAccountActivity.this, "public", publicKey);

                            AddressInformation param = new AddressInformation();
                            param.setAddressKey(publicKey);
                            Call<UserInformation> getProductAmount = manager.getInformationProfileByAddress(param);
                            getProductAmount.enqueue(new Callback<UserInformation>() {
                                @Override
                                public void onResponse(Call<UserInformation> call, Response<UserInformation> response) {
                                    try {
                                        if (response.code() == HttpStatus.SC_OK) {
                                            UserInformation obj = response.body();

                                            JSONObject objs = new JSONObject();
                                            objs.put("addressKey", obj.getAddress());
                                            objs.put("name", obj.getName());
                                            objs.put("phone", obj.getPhone());
                                            objs.put("city", obj.getKabupaten());
                                            objs.put("province", obj.getProvinsi());
                                            objs.put("fulladdress", obj.getAddress());
                                            objs.put("long", obj.getLon());
                                            objs.put("lat", obj.getLon());

                                            Intib._getInstance().updateLocalValue(RecoveryAccountActivity.this, Constant.PROFILE, objs.toString());
                                            Intent intent = new Intent(RecoveryAccountActivity.this,SetPinActivity.class);
                                            intent.putExtra(Constant.EXTRA_TYPE,3);
                                            startActivity(intent);
                                        } else {
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<UserInformation> call, Throwable t) {
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                return;
            }
        }
        mScannerView.startCamera();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }
}
