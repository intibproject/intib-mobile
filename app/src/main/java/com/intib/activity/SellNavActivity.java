package com.intib.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.intib.R;
import com.intib.activity.adapter.NavSendPagerAdapter;
import com.intib.activity.fragment.NavSellPagerAdapter;
import com.intib.util.ToolbarPagerIndicator;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SellNavActivity extends AppCompatActivity {

    @BindView(R.id.magic_indicator2)
    MagicIndicator magicIndicator2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private static final String[] CHANNEL = new String[]{"Jual Beras"};
    private static final String[] CHANNELS = new String[]{"Jual Beras","Daftar Jual","Persetujuan Jual Beras"};
    private List<String> menuList = Arrays.asList(CHANNEL);
    private List<String> menuLists = Arrays.asList(CHANNELS);
    private List<String> mDataList = menuLists;
    private NavSellPagerAdapter mExamplePagerAdapter;

    public void setmDataList() {
        this.mDataList = this.menuList;
    }
    public void setmDataLists() {
        this.mDataList = this.menuLists;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_dashboard_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ToolbarPagerIndicator.initMagicIndicator(this, mDataList, viewPager, magicIndicator2);
        mExamplePagerAdapter = new NavSellPagerAdapter(getSupportFragmentManager(), mDataList);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                viewPager.setCurrentItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setAdapter(mExamplePagerAdapter);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
