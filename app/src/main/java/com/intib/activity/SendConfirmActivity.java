package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.PointInformation;
import com.intib.model.SendInformation;
import com.intib.util.Constant;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendConfirmActivity extends AppCompatActivity {

    @BindView(R.id.tvTujuan)
    CustomTextView tvTujuan;
    @BindView(R.id.tvDetil)
    CustomTextView tvDetil;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvNominal)
    CustomTextView tvNominal;
    @BindView(R.id.tvPrice)
    CustomTextView tvPrice;
    @BindView(R.id.btnSend)
    RelativeLayout btnSend;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @BindView(R.id.tvWallet)
    CustomTextView tvWallet;

    private Integer amount = 0;
    private Integer price = 0;
    private String receiver = "";
    private String receiverAddress = "";

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_confirm);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        setAmount(Integer.parseInt(intent.getStringExtra("amount")));
        setPrice(Integer.parseInt(intent.getStringExtra("price")));
        setReceiver(intent.getStringExtra("receiver"));
        setReceiverAddress(intent.getStringExtra("receiverAddress"));

        tvTujuan.setText(intent.getStringExtra("receiver"));
        tvNominal.setText(intent.getStringExtra("amount"));
        tvPrice.setText(String.format("Rp. %,d.-", Integer.parseInt(intent.getStringExtra("price"))));
        tvDate.setText(dateformat.format(new Date()));
        tvDetil.setText(intent.getStringExtra("title"));
        tvWallet.setText(Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(this)));
    }

    @OnClick(R.id.btnSend)
    public void onViewClicked() {

        String jsonStr = Intib._getInstance().getLocalValue(this, Constant.PROFILE) != null ?
                Intib._getInstance().getLocalValue(this, Constant.PROFILE).toString() : "";

        String kabupaten = "";
        String provinsi = "";
        double lat = 0;
        double lon = 0;

        if (!jsonStr.equalsIgnoreCase("")) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(jsonStr);

                kabupaten = obj.getString("city");
                provinsi = obj.getString("province");
                lat = obj.getDouble("lat");
                lon = obj.getDouble("long");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SendInformation param = new SendInformation();
        param.setAmount(getAmount());
        param.setSender(Intib._getInstance().getPublicAddress(this));
        param.setReceiver(getReceiverAddress());
        param.setPrice(getPrice());
        param.setApproved(false);
        param.setKabupaten(kabupaten);
        param.setProvinsi(provinsi);
        param.setLat(lat);
        param.setLon(lon);
        //sender, receiver, amount, price, longitude, latitude, provinsi, kabupaten, created_at, approved
        Call<ResponseBody> checkAccountByUsername = manager.createTransaction(param);
        checkAccountByUsername.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == HttpStatus.SC_OK) {
                        Integer oldStock = Integer.parseInt(Intib._getInstance().getStock(SendConfirmActivity.this));
                        Integer newStock = oldStock - getAmount();
                        Intib._getInstance().updateLocalValue(SendConfirmActivity.this, Constant.STOCK, String.valueOf(newStock));
                        Intent intent = new Intent(SendConfirmActivity.this, DashboardNavActivity.class);
                        startActivity(intent);
                    } else {
                        Intib._getInstance().showError(SendConfirmActivity.this, "Kirim Beras Gagal");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Intib._getInstance().showError(SendConfirmActivity.this, "Kirim Beras Gagal");
            }
        });
    }
}