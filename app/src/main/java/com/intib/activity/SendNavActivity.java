package com.intib.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.intib.R;
import com.intib.activity.adapter.MenuAdapter;
import com.intib.activity.adapter.NavPagerAdapter;
import com.intib.activity.adapter.NavSendPagerAdapter;
import com.intib.util.ToolbarPagerIndicator;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendNavActivity extends AppCompatActivity {

    @BindView(R.id.magic_indicator2)
    MagicIndicator magicIndicator2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private static final String[] CHANNEL = new String[]{"Kirim via username"};
    private static final String[] CHANNELS = new String[]{"Kirim via username","Kirim via QR Code"};
    private List<String> menuList = Arrays.asList(CHANNEL);
    private List<String> menuLists = Arrays.asList(CHANNELS);
    private List<String> mDataList = menuLists;
    private NavSendPagerAdapter mExamplePagerAdapter;

    public void setmDataList() {
        this.mDataList = this.menuList;
    }
    public void setmDataLists() {
        this.mDataList = this.menuLists;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /*Dexter.withActivity(this)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        finish();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        finish();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();*/

        PackageManager pm = getPackageManager();

        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            setContentView(R.layout.fragment_dashboard_main);
            ButterKnife.bind(this);

            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            ToolbarPagerIndicator.initMagicIndicator(this, mDataList, viewPager, magicIndicator2);
            mExamplePagerAdapter = new NavSendPagerAdapter(getSupportFragmentManager(), mDataList);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    viewPager.setCurrentItem(i);
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

            viewPager.setAdapter(mExamplePagerAdapter);
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
