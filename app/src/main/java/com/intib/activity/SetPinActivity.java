package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.util.Constant;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetPinActivity extends AppCompatActivity {


    @BindView(R.id.etSetPin)
    CustomEditText etSetPin;
    @BindView(R.id.tvSetPin_header)
    CustomTextView tvSetPinHeader;
    @BindView(R.id.etConfirmPin)
    CustomEditText etConfirmPin;
    @BindView(R.id.tvConfirmPin_header)
    CustomTextView tvConfirmPinHeader;
    @BindView(R.id.etLoginPin)
    CustomEditText etLoginPin;
    @BindView(R.id.tvLoginPin_header)
    CustomTextView tvLoginPinHeader;
    @BindView(R.id.rlLogin)
    RelativeLayout rlLogin;
    @BindView(R.id.tvLogin)
    CustomTextView tvLogin;
    @BindView(R.id.btnSubmitPin)
    LinearLayout btnSubmitPin;
    @BindView(R.id.tvHeader)
    CustomTextView tvHeader;
    private Integer mType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mType = extras.getInt(Constant.EXTRA_TYPE, Constant.SET_PIN);
        }
        initLayout(mType);
    }

    @OnClick(R.id.btnSubmitPin)
    public void onViewClicked() {
        if (mType == Constant.LOGIN_PIN) {
            loginAct();
        } else if (mType == Constant.SET_PIN) {
            registAct();
        } else if (mType == Constant.LOGIN_PVKey) {
            loginPvAct();
        } else if (mType == 3) {
            recoveryAccount();
        }
    }

    private void initLayout(Integer type) {
        if (type == Constant.LOGIN_PIN) {
            etSetPin.setVisibility(View.GONE);
            tvSetPinHeader.setVisibility(View.GONE);
            tvConfirmPinHeader.setVisibility(View.GONE);
            etConfirmPin.setVisibility(View.GONE);
            rlLogin.setVisibility(View.VISIBLE);
            tvLogin.setText("MASUK");
            tvHeader.setText("Masukkan PIN");
        } else if (mType == Constant.SET_PIN) {
            etSetPin.setVisibility(View.VISIBLE);
            tvSetPinHeader.setVisibility(View.VISIBLE);
            tvConfirmPinHeader.setVisibility(View.VISIBLE);
            etConfirmPin.setVisibility(View.VISIBLE);
            rlLogin.setVisibility(View.GONE);
            tvLogin.setText("SIMPAN PIN");
            tvHeader.setText("Atur PIN Baru");
        } else if (mType == Constant.LOGIN_PVKey) {
            etSetPin.setVisibility(View.GONE);
            tvSetPinHeader.setVisibility(View.GONE);
            tvConfirmPinHeader.setVisibility(View.GONE);
            etConfirmPin.setVisibility(View.GONE);
            rlLogin.setVisibility(View.VISIBLE);
            tvLogin.setText("MASUK");
            tvHeader.setText("Masukkan PIN");
        } else if (mType == 3) {
            etSetPin.setVisibility(View.GONE);
            tvSetPinHeader.setVisibility(View.GONE);
            tvConfirmPinHeader.setVisibility(View.GONE);
            etConfirmPin.setVisibility(View.GONE);
            rlLogin.setVisibility(View.VISIBLE);
            tvLogin.setText("MASUK");
            tvHeader.setText("Masukkan PIN");
        }
    }

    private void loginAct() {
        String pin = String.valueOf(etLoginPin.getText());

        if (pin.equalsIgnoreCase("")) {
            Intib._getInstance().showError(this, "Harap Masukkan PIN Terlebih Dahulu");
        } else {
            boolean res = Intib._getInstance().checkLogin(this, pin);
            if (res) {
                Intib._getInstance().showSuccess(this, "Login Berhasil");
                Intent intent = new Intent(SetPinActivity.this, DashboardNavActivity.class);
                startActivity(intent);
            } else {
                Intib._getInstance().showError(this, "Login Gagal, Harap Periksa Kembali PIN Anda");
            }
        }
    }

    private void registAct() {
        String pin = String.valueOf(etSetPin.getText());
        String confPin = String.valueOf(etConfirmPin.getText());

        if (pin.trim().equalsIgnoreCase("")) {
            Intib._getInstance().showError(this, "Harap Masukkan PIN Terlebih Dahulu");
        } else {
            if (pin.trim().equalsIgnoreCase(confPin.trim())) {
                if (Intib._getInstance().savePin(this, pin)) {
                    Intib._getInstance().showSuccess(this, "Atur PIN Berhasil");
                    Intent intent = new Intent(SetPinActivity.this, SetStockActivity.class);
                    startActivity(intent);
                }
            } else {
                Intib._getInstance().showError(this, "PIN Dan Konfirmasi PIN Tidak Sama");
            }
        }
    }

    private void loginPvAct() {
        String pin = String.valueOf(etLoginPin.getText());

        if (pin.equalsIgnoreCase("")) {
            Intib._getInstance().showError(this, "Harap Masukkan PIN Terlebih Dahulu");
        } else {
            boolean res = Intib._getInstance().checkLogin(this, pin);
            if (res) {
                Intent intent = new Intent(SetPinActivity.this, PrivateKeyActivity.class);
                startActivity(intent);
            } else {
                Intib._getInstance().showError(this, "PIN Salah, Harap Periksa Kembali PIN Anda");
            }
        }
    }

    private void recoveryAccount() {
        String pin = String.valueOf(etSetPin.getText());
        String confPin = String.valueOf(etConfirmPin.getText());

        if (pin.trim().equalsIgnoreCase("")) {
            Intib._getInstance().showError(this, "Harap Masukkan PIN Terlebih Dahulu");
        } else {
            if (pin.trim().equalsIgnoreCase(confPin.trim())) {
                if (Intib._getInstance().savePin(this, pin)) {
                    Intib._getInstance().showSuccess(this, "Atur PIN Berhasil");
                    Intent intent = new Intent(SetPinActivity.this, DashboardNavActivity.class);
                    startActivity(intent);
                }
            } else {
                Intib._getInstance().showError(this, "PIN Dan Konfirmasi PIN Tidak Sama");
            }
        }
    }
}
