package com.intib.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.PointInformation;
import com.intib.model.StockInformation;
import com.intib.util.Constant;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetStockActivity extends AppCompatActivity {

    @BindView(R.id.tvHeader)
    CustomTextView tvHeader;
    @BindView(R.id.etStock)
    CustomEditText etStock;
    @BindView(R.id.tvStock_header)
    CustomTextView tvStockHeader;
    @BindView(R.id.rlStock)
    RelativeLayout rlStock;
    @BindView(R.id.btnSubmitStock)
    LinearLayout btnSubmitStock;

    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_set_stock);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnSubmitStock)
    public void onViewClicked() {
        String stStock = etStock.getText().toString().trim();
        if (!stStock.equalsIgnoreCase("")) {
            Integer inStock = Integer.parseInt(stStock);
            if(inStock > 0){
                Intib._getInstance().updateLocalValue(this, Constant.STOCK,stStock);

                StockInformation stock = new StockInformation();
                stock.setAddressKey(Intib._getInstance().getPublicAddress(this));
                stock.setAmount(inStock);
                stock.setPrice(0);

                try {
                    Call<ResponseBody> setStock = manager.setStock(stock);
                    setStock.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == HttpStatus.SC_OK) {
                                Intib._getInstance().showSuccess(SetStockActivity.this, "Atur Stok Beras Berhasil");
                                Intent intent = new Intent(SetStockActivity.this, ProfileInformationActivity.class);
                                startActivity(intent);
                            }else{
                                Intib._getInstance().showError(SetStockActivity.this, "Gagal Atur Stok Beras");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Intib._getInstance().showError(SetStockActivity.this, "Gagal Atur Stok Beras");
                        }
                    });
                }catch (Exception e){
                    Log.e("intib",e.getMessage());
                }

                PointInformation param = new PointInformation();
                param.setAddressKey(Intib._getInstance().getPublicAddress(SetStockActivity.this));
                param.setPoint(100);

                try {
                    Call<ResponseBody> setStock = manager.addPoint(param);
                    setStock.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == HttpStatus.SC_OK) {
                            }else{
                                Intib._getInstance().showError(SetStockActivity.this,"Gagal Tambah Poin");
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }catch (Exception e){
                    Log.e("intib",e.getMessage());
                }


            }else{
                Intib._getInstance().showError(this, "Stok Beras Tidak Boleh Kosong!!!");
            }
        } else {
            Intib._getInstance().showError(this, "Stok Beras Tidak Boleh Kosong!!!");
        }
    }
}
