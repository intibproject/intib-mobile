package com.intib.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.fragment.InformationTourFragment;
import com.intib.manager.ApiManager;
import com.intib.model.UserInformation;
import com.intib.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;

  //ApiManager manager = Intib._getClient().create(ApiManager.class) ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*Call<List<UserInformation>> userInformationCall = manager.getUserInformation();
        userInformationCall.enqueue(new Callback<List<UserInformation>>() {
            @Override
            public void onResponse(Call<List<UserInformation>> call, Response<List<UserInformation>> response) {
                if(response.body() != null) {
                    for (UserInformation size : response.body()) {
                        System.out.println(size.getName());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<UserInformation>> call, Throwable t) {

            }
        });*/

        final Class aClass ;
        final Intent i;
        if (Intib._getInstance().isRegistered(this)) {
            if(Intib._getInstance().getStock(this).equalsIgnoreCase("0")){
                i = new Intent(SplashActivity.this, SetStockActivity.class);
            }else {
                aClass = new SetPinActivity().getClass();
                i = new Intent(SplashActivity.this, aClass);
                i.putExtra(Constant.EXTRA_TYPE, Constant.LOGIN_PIN);
            }
        }else{
            aClass = new InformationTourActivity().getClass();
            i = new Intent(SplashActivity.this, aClass);
            i.putExtra(Constant.EXTRA_TYPE, Constant.SET_PIN);
        }


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
}
