package com.intib.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.util.button.CustomButton;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebIntibActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnGenerate)
    CustomButton btnGenerate;
    @BindView(R.id.tvCountdown)
    CustomTextView tvCountdown;
    @BindView(R.id.tvAccessCode)
    CustomTextView tvAccessCode;
    @BindView(R.id.txtLabel1)
    CustomTextView txtLabel1;
    @BindView(R.id.txtLabel2)
    CustomTextView txtLabel2;
    @BindView(R.id.btnCopy)
    LinearLayout btnCopy;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    private static final String FORMAT = "%02d:%02d:%02d";
    int seconds, minutes;
    String codeAccess = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_intib);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_chevron_left);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Web Intib");
    }

    private void initLayout(int code) {
        if (code == 0) {
            btnGenerate.setVisibility(View.GONE);
            txtLabel1.setVisibility(View.GONE);
            tvAccessCode.setVisibility(View.VISIBLE);
            tvCountdown.setVisibility(View.VISIBLE);
            txtLabel2.setVisibility(View.VISIBLE);
            btnCopy.setVisibility(View.VISIBLE);
        } else {
            btnGenerate.setVisibility(View.VISIBLE);
            txtLabel1.setVisibility(View.VISIBLE);
            tvAccessCode.setVisibility(View.GONE);
            tvCountdown.setVisibility(View.GONE);
            txtLabel2.setVisibility(View.GONE);
            btnCopy.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.btnGenerate, R.id.btnCopy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGenerate:
                AddressInformation param = new AddressInformation();
                param.setAddressKey(Intib._getInstance().getPublicAddress(this));
                Call<ResponseBody> getLatestStock = manager.generateTokenWeb(param);
                getLatestStock.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.code() == HttpStatus.SC_OK) {
                                String jsonData = response.body().string();
                                JSONObject jsonObj = new JSONObject(jsonData);
                                String token = jsonObj.getString("WebToken");
                                tvAccessCode.setText(token);
                                codeAccess = token;
                                initLayout(0);
                                new CountDownTimer(5000, 1000) { // adjust the milli seconds here
                                    public void onTick(long millisUntilFinished) {
                                        tvCountdown.setText("" + String.format(FORMAT,
                                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                    }

                                    public void onFinish() {
                                        initLayout(1);
                                    }
                                }.start();
                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
                break;
            case R.id.btnCopy:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Access Code", codeAccess);
                clipboard.setPrimaryClip(clip);
                Intib._getInstance().showSuccess(this, "Kode Akses telah di salin");
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
