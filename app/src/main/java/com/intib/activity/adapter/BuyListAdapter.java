package com.intib.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.Intib;
import com.intib.R;
import com.intib.model.BuyInformation;
import com.intib.model.BuyNotifInformation;
import com.intib.util.CustomDateFormat;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class BuyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BuyNotifInformation> buyInformations;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTotal, tvAmount, tvDate, tvPurposeTo;
        public ImageView ivStatusTransactions;

        public MyViewHolder(View view) {
            super(view);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvTotal = (TextView) view.findViewById(R.id.tvTotal);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            tvPurposeTo = (TextView) view.findViewById(R.id.tvPurposeTo);
            ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
        }
    }

    public BuyListAdapter(List<BuyNotifInformation> sellList) {
        this.buyInformations = sellList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_buy_list, parent, false);

        return new BuyListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof BuyListAdapter.MyViewHolder) {
            BuyNotifInformation buyInformation = buyInformations.get(position);

            ((BuyListAdapter.MyViewHolder) viewHolder).tvDate.setText(CustomDateFormat._getInstance().getDateTime(Long.parseLong(buyInformation.getCreatedAt().toString())));
            ((MyViewHolder) viewHolder).tvAmount.setText(Intib._getInstance().formatAmountWithKilo(buyInformation.getAmount()));
            ((MyViewHolder) viewHolder).tvTotal.setText(Intib._getInstance().RupiahFormat(Double.valueOf(buyInformation.getPrice()*buyInformation.getQuantity())));
            ((MyViewHolder) viewHolder).tvPurposeTo.setText(buyInformation.getUsername());
            ((MyViewHolder) viewHolder).ivStatusTransactions.setBackgroundResource(R.drawable.ic_sell);
        }
    }

    @Override
    public int getItemCount() {
        return buyInformations == null ? 0 : buyInformations.size();
    }
}