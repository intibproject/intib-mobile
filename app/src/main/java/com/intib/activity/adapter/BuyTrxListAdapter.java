package com.intib.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.Intib;
import com.intib.R;
import com.intib.model.BuyInformation;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class BuyTrxListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
private List<BuyInformation> buyInformations;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView tvStatus, tvTransactions, tvAmount, tvPurposeTo, tvDate, tvAmountRice;
    public ImageView ivStatusTransactions;

    public MyViewHolder(View view) {
        super(view);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        tvTransactions = (TextView) view.findViewById(R.id.tvTransactions);
        tvPurposeTo = (TextView) view.findViewById(R.id.tvPurposeTo);
        tvStatus = (TextView) view.findViewById(R.id.tvStatus);
        tvAmount = (TextView) view.findViewById(R.id.tvAmount);
        ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
        tvAmountRice = (TextView) view.findViewById(R.id.tvAmountRice);
    }
}

    public BuyTrxListAdapter(List<BuyInformation> sellList) {
        this.buyInformations = sellList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_get_rice, parent, false);

        return new BuyTrxListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof SellRiceTrxListAdapter.MyViewHolder) {
            BuyInformation buyInformation = buyInformations.get(position);

            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvDate.setText(buyInformation.getDatetime());
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvAmountRice.setText(Intib._getInstance().formatAmountWithKilo(buyInformation.getAmount()));
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvAmount.setText(Intib._getInstance().RupiahFormat(Double.valueOf(buyInformation.getTotal())));
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvStatus.setText(buyInformation.getStatus());
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvTransactions.setText(buyInformation.getLabelTrx());
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).ivStatusTransactions.setBackgroundResource(R.drawable.ic_sell);
        }
    }

    @Override
    public int getItemCount() {
        return buyInformations == null ? 0 : buyInformations.size();
    }
}