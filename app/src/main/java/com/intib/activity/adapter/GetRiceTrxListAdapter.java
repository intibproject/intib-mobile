package com.intib.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.Intib;
import com.intib.R;
import com.intib.model.GetRiceInformation;
import com.intib.util.CustomDateFormat;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/28/17.
 */

public class GetRiceTrxListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetRiceInformation> riceInformations;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStatus, tvTransactions, tvAmount, tvPurposeTo, tvDate;
        public ImageView ivStatusTransactions;

        public MyViewHolder(View view) {
            super(view);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvTransactions = (TextView) view.findViewById(R.id.tvTransactions);
            tvPurposeTo = (TextView) view.findViewById(R.id.tvPurposeTo);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
        }
    }

    public GetRiceTrxListAdapter(List<GetRiceInformation> riceList) {
        this.riceInformations = riceList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_get_rice, parent, false);

        return new GetRiceTrxListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyViewHolder) {
            GetRiceInformation riceInformation = riceInformations.get(position);

            ((MyViewHolder) viewHolder).tvDate.setText( CustomDateFormat._getInstance().getDateTime(Long.parseLong(riceInformation.getDatetime())));
            ((MyViewHolder) viewHolder).tvAmount.setText(Intib._getInstance().formatAmountWithKilo(riceInformation.getAmount()));
            ((MyViewHolder) viewHolder).tvStatus.setText(riceInformation.getStatus());
            ((MyViewHolder) viewHolder).tvPurposeTo.setText(riceInformation.getSenderName());
        }
    }

    @Override
    public int getItemCount() {
        return riceInformations == null ? 0 : riceInformations.size();
    }
}
