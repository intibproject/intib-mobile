package com.intib.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.Intib;
import com.intib.R;
import com.intib.model.GetRiceInformation;
import com.intib.model.HistoryTrxDetailInformation;
import com.intib.model.HistoryTrxInformation;
import com.intib.util.CustomDateFormat;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/28/17.
 */

public class HistTrxListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HistoryTrxDetailInformation> histInformations;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStatus, tvTransactions, tvAmount, tvPurposeTo, tvDate, tvTotal;
        public ImageView ivStatusTransactions;

        public MyViewHolder(View view) {
            super(view);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvTransactions = (TextView) view.findViewById(R.id.tvTransactions);
            tvPurposeTo = (TextView) view.findViewById(R.id.tvPurposeTo);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
            tvTotal=  (TextView) view.findViewById(R.id.tvTotal);
        }
    }

    public HistTrxListAdapter(List<HistoryTrxDetailInformation> histInformations) {
        this.histInformations = histInformations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hist_trx, parent, false);

        return new HistTrxListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyViewHolder) {
            HistoryTrxDetailInformation histInformation = histInformations.get(position);

            ((MyViewHolder) viewHolder).tvDate.setText( CustomDateFormat._getInstance().getDateTime(Long.parseLong(histInformation.getCreatedAt().toString())));
            ((MyViewHolder) viewHolder).tvAmount.setText(Intib._getInstance().formatAmountWithKilo(histInformation.getAmount()));
            ((MyViewHolder) viewHolder).tvTotal.setText(Intib._getInstance().RupiahFormat(Double.valueOf(histInformation.getPrice()*histInformation.getAmount())));
            if(histInformation.getAddressKey().trim().equals(histInformation.getSender().trim())){
                ((MyViewHolder) viewHolder).tvTransactions.setText("Kirim Beras Ke");
                ((MyViewHolder) viewHolder).tvPurposeTo.setText(histInformation.getReceiverUsername());
            }else if(histInformation.getAddressKey().trim().equals(histInformation.getReceiver().trim())){
                ((MyViewHolder) viewHolder).tvTransactions.setText("Terima Beras Dari");
                ((MyViewHolder) viewHolder).tvPurposeTo.setText(histInformation.getSenderUsername());
            }else{
                ((MyViewHolder) viewHolder).tvStatus.setText("-");
                ((MyViewHolder) viewHolder).tvPurposeTo.setText("-");
            }
        }
    }

    @Override
    public int getItemCount() {
        return histInformations == null ? 0 : histInformations.size();
    }
}
