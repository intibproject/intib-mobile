package com.intib.activity.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.R;

import java.util.ArrayList;

import nl.psdcompany.duonavigationdrawer.views.DuoOptionView;

/**
 * Created by Fadhilla Eka Hentino on 11/19/17.
 */

public class MenuAdapter extends BaseAdapter {
    private ArrayList<String> mOptions = new ArrayList<>();
    private ArrayList<Integer> mOptionsImage = new ArrayList<>();
    private ArrayList<DuoOptionView> mOptionViews = new ArrayList<>();
    Activity activity;
    private LayoutInflater inflater;

    public MenuAdapter(Activity activity, ArrayList<String> options) {
        mOptions = options;
        if (mOptionsImage.size() == 0) {
            mOptionsImage.add(R.drawable.menu_icon_menu_utama);
            mOptionsImage.add(R.drawable.menu_icon_akun);
            mOptionsImage.add(R.drawable.menu_icon_menu_utama);
            mOptionsImage.add(R.drawable.menu_icon_menu_utama);
        }
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);
    }

    public void setViewSelected(int position, boolean selected) {
        for (int i = 0; i < mOptionViews.size(); i++) {
            if (i == position) {
                mOptionViews.get(i).setSelected(selected);
            } else {
                mOptionViews.get(i).setSelected(!selected);
            }
        }
    }

    @Override
    public int getCount() {
        return mOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return mOptions.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final String option = mOptions.get(position);
        final Integer optionImage = mOptionsImage.get(position);

        convertView = inflater.inflate(R.layout.item_menu_adapter, null);

        TextView txt = (TextView) convertView.findViewById(R.id.tvName);
        ImageView imgST = (ImageView) convertView.findViewById(R.id.ivStatusTransactions);
        ImageView ivNotif = (ImageView) convertView.findViewById(R.id.ivNotif);

        txt.setText(option);
        imgST.setImageResource(optionImage);

        if (position == 0){
            ivNotif.setVisibility(View.GONE);
        } else if (position == 2){
            ivNotif.setVisibility(View.GONE);
        }

        return convertView;
    }
}
