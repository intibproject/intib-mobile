package com.intib.activity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.intib.activity.fragment.GetRiceListFragment;
import com.intib.activity.fragment.GetRiceQRFragment;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/26/17.
 */

public class NavGetRicePagerAdapter extends FragmentPagerAdapter {
    private List<String> mDataList;

    public NavGetRicePagerAdapter(FragmentManager fm, List<String> mDataList) {
        super(fm);
        this.mDataList = mDataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new GetRiceQRFragment();
            case 1:
                return new GetRiceListFragment();
            default:
                return null;
        }

    }
}
