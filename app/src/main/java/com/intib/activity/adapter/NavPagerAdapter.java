package com.intib.activity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.intib.activity.fragment.DashboardMainFragment;
import com.intib.activity.fragment.HistoryTrxListFragment;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/19/17.
 */

public class NavPagerAdapter extends FragmentPagerAdapter {
    private List<String> mDataList;

    public NavPagerAdapter(FragmentManager fm, List<String> mDataList) {
        super(fm);
        this.mDataList = mDataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new DashboardMainFragment();
            case 1:
                return new HistoryTrxListFragment();
            default:
                return null;
        }

    }
}
