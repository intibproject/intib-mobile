package com.intib.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intib.Intib;
import com.intib.R;
import com.intib.model.SellInformation;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/29/17.
 */

public class SellRiceTrxListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SellInformation> sellInformations;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStatus, tvTransactions, tvAmount, tvTotal, tvDate, tvAmountRice;
        public ImageView ivStatusTransactions;

        public MyViewHolder(View view) {
            super(view);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvTransactions = (TextView) view.findViewById(R.id.tvTransactions);
            tvTotal = (TextView) view.findViewById(R.id.tvTotal);
            ivStatusTransactions = (ImageView) view.findViewById(R.id.ivStatusTransactions);
            tvAmountRice = (TextView) view.findViewById(R.id.tvAmount);
        }
    }

    public SellRiceTrxListAdapter(List<SellInformation> sellList) {
        this.sellInformations = sellList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sell_rice, parent, false);

        return new SellRiceTrxListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof SellRiceTrxListAdapter.MyViewHolder) {
            SellInformation sellInformation = sellInformations.get(position);

            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvDate.setText(sellInformation.getDatetime());
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvAmountRice.setText(Intib._getInstance().formatAmountWithKilo(sellInformation.getAmount()));
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvTotal.setText(Intib._getInstance().RupiahFormat(Double.valueOf(sellInformation.getPrice()*sellInformation.getAmount())));
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).tvTransactions.setText("Jual Beras");
            ((SellRiceTrxListAdapter.MyViewHolder) viewHolder).ivStatusTransactions.setBackgroundResource(R.drawable.ic_sell);
        }
    }

    @Override
    public int getItemCount() {
        return sellInformations == null ? 0 : sellInformations.size();
    }
}

