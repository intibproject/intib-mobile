package com.intib.activity.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.adapter.BuyListAdapter;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.BuyInformation;
import com.intib.util.DividerItemDecoration;
import com.intib.util.RecyclerItemClickListener;
import com.intib.util.fonts.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BuyListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeToRefreshLayout;
    @BindView(R.id.tvTrans)
    CustomTextView tvTrans;
    Unbinder unbinder;
    private List<BuyInformation> buyInformations = new ArrayList<BuyInformation>();

    BuyListAdapter listBuyRiceAdapter;
    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buy_list, container, false);

        unbinder = ButterKnife.bind(this, v);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());

        swipeToRefreshLayout.setOnRefreshListener(this);

        new BuyListFragment.ReportTaskBuy().execute();

        revList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), revList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        /*Intent receiptIntent = new Intent(getActivity(), ReceiptGetRiceActivity.class);
                        receiptIntent.putExtra("date",
                                CustomDateFormat._getInstance().getDateYR(Long.parseLong(riceInformations.get(i).getDatetime())));
                        receiptIntent.putExtra("time",
                                CustomDateFormat._getInstance().getDateHR(Long.parseLong(riceInformations.get(i).getDatetime())));
                        receiptIntent.putExtra("status",
                                riceInformations.get(i).getStatus());
                        receiptIntent.putExtra("receiver",
                                riceInformations.get(i).getReceiverName());
                        receiptIntent.putExtra("trxId",
                                riceInformations.get(i).getTrxId());
                        startActivity(receiptIntent);*/
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        new BuyListFragment.ReportTaskBuy().execute();

        //listBuyRiceAdapter = new BuyListAdapter(buyInformations);
        revList.setAdapter(listBuyRiceAdapter);

        if (buyInformations.size() > 0) {
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new BuyListFragment.ReportTaskBuy().execute();
    }

    public class ReportTaskBuy extends AsyncTask<String, Void, String> {

        public ReportTaskBuy() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buyInformations.clear();
            Intib._getInstance().getInternalDB(getContext()).resetSellRice();
        }

        @Override
        protected String doInBackground(String... strings) {
            final SqlLiteDB db = new SqlLiteDB(getContext());

            /*BuyInformation obj = new BuyInformation();
            obj.setTrxId("001");
            obj.setSellerName("fadhil");
            obj.setAmount(1000);
            obj.setPrice(5000);
            obj.setTotal(5000000);
            obj.setStatus("PENDING");
            obj.setDatetime("1511860272");
            obj.setLabelTrx("Jual Beras");
            buyInformations.add(obj);
            db.addTrxSellRice(buyInformations);

            obj = new BuyInformation();
            obj.setTrxId("002");
            obj.setSellerName("fadhil");
            obj.setAmount(50);
            obj.setPrice(3000);
            obj.setTotal(150000);
            obj.setStatus("APPROVED");
            obj.setDatetime("1511860272");
            obj.setLabelTrx("Jual Beras");
            buyInformations.add(obj);
            db.addTrxSellRice(buyInformations);*/
            /*try {
                Call<List<GetRiceInformation>> userInformationCall = manager.getListGetRice();
                userInformationCall.enqueue(new Callback<List<GetRiceInformation>>() {
                    @Override
                    public void onResponse(Call<List<GetRiceInformation>> call, Response<List<GetRiceInformation>> response) {
                        if(response.body() != null) {
                            riceInformations = response.body();
                            if(riceInformations.size() > 0){
                                db.addTrxGetRice(riceInformations);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<List<GetRiceInformation>> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            swipeToRefreshLayout.setRefreshing(false);
            //listBuyRiceAdapter = new BuyListAdapter(buyInformations);
            revList.setAdapter(listBuyRiceAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
