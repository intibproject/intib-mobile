package com.intib.activity.fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.ReceiptBuyActivity;
import com.intib.activity.adapter.BuyListAdapter;
import com.intib.activity.adapter.SellRiceTrxListAdapter;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.BuyInformation;
import com.intib.model.BuyNotifInformation;
import com.intib.model.BuyNotifParentInformation;
import com.intib.model.BuyParamInformation;
import com.intib.model.SellInformation;
import com.intib.model.SellTransactionDetailInformation;
import com.intib.model.SellTransactionInformation;
import com.intib.util.CustomDateFormat;
import com.intib.util.DividerItemDecoration;
import com.intib.util.RecyclerItemClickListener;
import com.intib.util.fonts.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuyTrxListFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeToRefreshLayout;
    @BindView(R.id.tvTrans)
    CustomTextView tvTrans;
    Unbinder unbinder;
    private List<BuyNotifInformation> buyInformations = new ArrayList<BuyNotifInformation>();

    BuyListAdapter listBuyRiceAdapter;
    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buy_list, container, false);

        unbinder = ButterKnife.bind(this, v);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());

        swipeToRefreshLayout.setOnRefreshListener(this);

        new BuyTrxListFragment.ReportTaskBuyTrx().execute();

        revList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), revList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Intent receiptIntent = new Intent(getActivity(), ReceiptBuyActivity.class);
                        receiptIntent.putExtra("date",
                                CustomDateFormat._getInstance().getDateTime(Long.parseLong(buyInformations.get(i).getCreatedAt().toString())));
                        receiptIntent.putExtra("sender",
                                buyInformations.get(i).getUsername());
                        receiptIntent.putExtra("phone",
                                buyInformations.get(i).getPhone());
                        receiptIntent.putExtra("trxId",
                                String.valueOf(buyInformations.get(i).getRequestId()));
                        receiptIntent.putExtra("amount",
                                buyInformations.get(i).getAmount());
                        receiptIntent.putExtra("price",
                                buyInformations.get(i).getPrice());
                        receiptIntent.putExtra("total",
                                buyInformations.get(i).getPrice()*buyInformations.get(i).getAmount());
                        startActivity(receiptIntent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        buyInformations = Intib._getInstance().getInternalDB(getContext()).getAllTransactionBuy();

        if(buyInformations.size() <= 0){
            new BuyTrxListFragment.ReportTaskBuyTrx().execute();
        }

        listBuyRiceAdapter = new BuyListAdapter(buyInformations);
        revList.setAdapter(listBuyRiceAdapter);

        if (buyInformations.size() > 0) {
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new BuyTrxListFragment.ReportTaskBuyTrx().execute();
    }

    public class ReportTaskBuyTrx extends AsyncTask<String, Void, String> {

        public ReportTaskBuyTrx() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buyInformations.clear();
            Intib._getInstance().getInternalDB(getContext()).resetBuyRice();
        }

        @Override
        protected String doInBackground(String... strings) {
            final SqlLiteDB db = new SqlLiteDB(getContext());

            try {
                BuyParamInformation param = new BuyParamInformation();
                param.setBuyer(Intib._getInstance().getPublicAddress(getContext()));
                param.setProcessedFilter(true);
                param.setProcessed(false);
                Call<BuyNotifParentInformation> getTransaction = manager.buyNotifList(param);
                getTransaction.enqueue(new Callback<BuyNotifParentInformation>() {
                    @Override
                    public void onResponse(Call<BuyNotifParentInformation> call, Response<BuyNotifParentInformation> response) {
                        if(response.body() != null) {
                            BuyNotifParentInformation obj = response.body();
                            for(BuyNotifInformation objDetail : obj.getInfo()){

                                buyInformations.add(objDetail);
                            }
                            db.addTrxBuy(buyInformations);
                            listBuyRiceAdapter = new BuyListAdapter(buyInformations);
                            revList.setAdapter(listBuyRiceAdapter);

                            if (buyInformations.size() > 0){
                                tvTrans.setVisibility(View.GONE);
                            } else {
                                tvTrans.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<BuyNotifParentInformation> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            swipeToRefreshLayout.setRefreshing(false);
            listBuyRiceAdapter = new BuyListAdapter(buyInformations);
            revList.setAdapter(listBuyRiceAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}