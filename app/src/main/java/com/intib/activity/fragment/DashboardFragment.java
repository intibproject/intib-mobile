package com.intib.activity.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.JsonParseException;
import com.intib.Intib;
import com.intib.R;
import com.intib.activity.BuyNavActivity;
import com.intib.activity.GetRiceNavActivity;
import com.intib.activity.ProfileInformationActivity;
import com.intib.activity.SellNavActivity;
import com.intib.activity.SendNavActivity;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.util.ChildAnimationExample;
import com.intib.util.Constant;
import com.intib.util.SliderLayout;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment implements BaseSliderView.OnSliderClickListener {
    Unbinder unbinder;
    @BindView(R.id.pp)
    ImageView pp;
    @BindView(R.id.TextView01)
    TextView TextView01;
    @BindView(R.id.TextView03)
    TextView TextView03;

    @BindView(R.id.tableRow1)
    TableRow tableRow1;
    @BindView(R.id.btnKirim)
    ImageButton btnKirim;
    @BindView(R.id.btnTerima)
    ImageButton btnTerima;
    @BindView(R.id.btnJual)
    ImageButton btnJual;
    @BindView(R.id.btnBeli)
    ImageButton btnBeli;

    @BindView(R.id.slider)
    SliderLayout slider;
    List<String> bannerList;
    @BindView(R.id.tvStock)
    CustomTextView tvStock;
    @BindView(R.id.tbMenu)
    TableLayout tbMenu;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    @BindView(R.id.tvUsername)
    CustomTextView tvUsername;
    @BindView(R.id.tvAddress)
    CustomTextView tvAddress;
    @BindView(R.id.layProfile)
    LinearLayout layProfile;
    @BindView(R.id.tvSell)
    CustomTextView tvSell;

    private static int RESULT_LOAD_IMAGE = 1;
    @BindView(R.id.tvPoints)
    CustomTextView tvPoints;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        unbinder = ButterKnife.bind(this, v);


        bannerList = new ArrayList<>();
        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("banner_ads1", R.drawable.banner_ads);
        file_maps.put("banner_ads2", R.drawable.banner2);
        file_maps.put("banner_ads3", R.drawable.banner3);

        int i = 0;
        for (String name : file_maps.keySet()) {

            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            slider.addSlider(textSliderView);

            i++;
        }

        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new ChildAnimationExample());
        slider.setDuration(4000);

        slider.getPagerIndicator().setDefaultIndicatorColor(Color.parseColor("#000000"), Color.parseColor("#7f8c8d"));

        tvStock.setText(Intib._getInstance().getStock(getContext()));
        String jsonStr = Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE) != null ?
                Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE).toString() : "";

        String username = "";
        String address = "";
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonStr);
            username = obj.getString("username");
            address = obj.getString("city").concat(",").concat(obj.getString("province"));
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (username.equalsIgnoreCase("")) {
            tvUsername.setText("<Isi UserNama Anda>");
        } else {
            tvUsername.setText(username);
        }

        if (address.equalsIgnoreCase("")) {
            tvAddress.setText("<Isi Lokasi Anda>");
        } else {
            tvAddress.setText(address);
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        AddressInformation param = new AddressInformation();
        param.setAddressKey(Intib._getInstance().getPublicAddress(getContext()));
        Call<ResponseBody> getLatestStock = manager.getLatestStock(param);
        getLatestStock.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == HttpStatus.SC_OK) {
                        String jsonData = response.body().string();
                        JSONObject jsonObj = new JSONObject(jsonData);
                        String stock = jsonObj.getString("Balance");
                        Intib._getInstance().updateLocalValue(getContext(), Constant.STOCK, stock);
                        tvStock.setText(Intib._getInstance().getStock(getContext()));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

        Call<ResponseBody> getProductAmount = manager.getProductAmount(param);
        getProductAmount.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == HttpStatus.SC_OK) {
                        String jsonData = response.body().string();
                        JSONObject jsonObj = new JSONObject(jsonData);
                        String amount = jsonObj.getString("amount");
                        tvSell.setText(amount);
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

        Call<ResponseBody> getPoint = manager.getPoint(param);
        getPoint.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == HttpStatus.SC_OK) {
                        String jsonData = response.body().string();
                        JSONObject jsonObj = new JSONObject(jsonData);
                        String amount = jsonObj.getString("point");
                        tvPoints.setText(amount);
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        String obj = slider.getBundle().get("extra").toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnKirim, R.id.btnTerima, R.id.btnJual, R.id.btnBeli, R.id.pp})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnKirim:
                intent = new Intent(getContext(), SendNavActivity.class);
                startActivity(intent);
                break;
            case R.id.btnTerima:
                intent = new Intent(getContext(), GetRiceNavActivity.class);
                startActivity(intent);
                break;
            case R.id.btnJual:
                intent = new Intent(getContext(), SellNavActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBeli:
                intent = new Intent(getContext(), BuyNavActivity.class);
                startActivity(intent);
                break;
            case R.id.pp:
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;
        }
    }

    @OnClick(R.id.layProfile)
    public void onViewClicked() {
        Intent intent = new Intent(getContext(), ProfileInformationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            pp.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }
}
