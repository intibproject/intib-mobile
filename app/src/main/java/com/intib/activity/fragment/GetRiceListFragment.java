package com.intib.activity.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.GetRiceConfirmActivity;
import com.intib.activity.adapter.GetRiceTrxListAdapter;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.PendingTransactionDetailInformation;
import com.intib.model.PendingTransactionInformation;
import com.intib.util.CustomDateFormat;
import com.intib.util.DividerItemDecoration;
import com.intib.util.RecyclerItemClickListener;
import com.intib.util.fonts.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetRiceListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeToRefreshLayout;
    @BindView(R.id.tvTrans)
    CustomTextView tvTrans;
    Unbinder unbinder;
    private List<GetRiceInformation> riceInformations = new ArrayList<GetRiceInformation>();

    GetRiceTrxListAdapter getRiceTrxListAdapter;
    ApiManager manager = Intib._getClient().create(ApiManager.class) ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_get_rice_list, container, false);

        unbinder = ButterKnife.bind(this, v);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());

        swipeToRefreshLayout.setOnRefreshListener(this);

        if (riceInformations.size() > 0){
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }

        revList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), revList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Intent receiptIntent = new Intent(getActivity(), GetRiceConfirmActivity.class);
                        receiptIntent.putExtra("date",
                                CustomDateFormat._getInstance().getDateTime(Long.parseLong(riceInformations.get(i).getDatetime())));
                        receiptIntent.putExtra("sender",
                                riceInformations.get(i).getSenderName());
                        receiptIntent.putExtra("trxId",
                                riceInformations.get(i).getTrxId());
                        receiptIntent.putExtra("amount",
                                riceInformations.get(i).getAmount());
                        receiptIntent.putExtra("price",
                                riceInformations.get(i).getPrice());
                        startActivity(receiptIntent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        riceInformations = Intib._getInstance().getInternalDB(getContext()).getAllTransactionGetRice();

        if(riceInformations.size() <= 0){
            new GetRiceListFragment.ReportTask().execute();
        }

        getRiceTrxListAdapter = new GetRiceTrxListAdapter(riceInformations);
        revList.setAdapter(getRiceTrxListAdapter);

        if (riceInformations.size() > 0){
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new GetRiceListFragment.ReportTask().execute();
    }

    public class ReportTask extends AsyncTask<String, Void, String> {

        public ReportTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            riceInformations.clear();
            Intib._getInstance().getInternalDB(getContext()).resetGetRice();
        }

        @Override
        protected String doInBackground(String... strings) {
            final SqlLiteDB db = new SqlLiteDB(getContext());

            /*GetRiceInformation obj = new GetRiceInformation();
            obj.setTrxId("001");
            obj.setSenderName("fadhil");
            obj.setReceiverName("rima");
            obj.setAmount(1000);
            obj.setPrice(5000);
            obj.setTotal(5000000);
            obj.setStatus("PENDING");
            obj.setDatetime("1511860272");
            riceInformations.add(obj);

            GetRiceInformation obj1 = new GetRiceInformation();
            obj1.setTrxId("002");
            obj1.setSenderName("eka");
            obj1.setReceiverName("rahma");
            obj1.setAmount(50);
            obj1.setPrice(3000);
            obj1.setTotal(150000);
            obj1.setStatus("APPROVED");
            obj1.setDatetime("1511860272");
            riceInformations.add(obj);

            db.addTrxGetRice(riceInformations);*/
            try {
                AddressInformation param = new AddressInformation();
                param.setAddressKey(Intib._getInstance().getPublicAddress(getContext()));
                Call<PendingTransactionInformation> pendingTransaction = manager.pendingTransaction(param);
                pendingTransaction.enqueue(new Callback<PendingTransactionInformation>() {
                    @Override
                    public void onResponse(Call<PendingTransactionInformation> call, Response<PendingTransactionInformation> response) {
                        if(response.body() != null) {
                            PendingTransactionInformation obj = response.body();
                            for(PendingTransactionDetailInformation objDetail : obj.getHistory()){
                                GetRiceInformation riceInformation = new GetRiceInformation();
                                riceInformation.setReceiverName(objDetail.getReceiverUserName());
                                riceInformation.setSenderName(objDetail.getSenderUserName());
                                riceInformation.setAmount(objDetail.getAmount());
                                riceInformation.setPrice(objDetail.getPrice());
                                riceInformation.setTrxId(String.valueOf(objDetail.getTransactionId()));
                                riceInformation.setDatetime(String.valueOf(objDetail.getCreatedAt()));
                                riceInformations.add(riceInformation);
                            }
                            db.addTrxGetRice(riceInformations);
                            getRiceTrxListAdapter = new GetRiceTrxListAdapter(riceInformations);
                            revList.setAdapter(getRiceTrxListAdapter);

                            if (riceInformations.size() > 0){
                                tvTrans.setVisibility(View.GONE);
                            } else {
                                tvTrans.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<PendingTransactionInformation> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            swipeToRefreshLayout.setRefreshing(false);
            getRiceTrxListAdapter = new GetRiceTrxListAdapter(riceInformations);
            revList.setAdapter(getRiceTrxListAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
