package com.intib.activity.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.intib.Intib;
import com.intib.R;
import com.intib.util.Constant;
import com.intib.util.button.CustomButton;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetRiceQRFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.tvWallet)
    CustomTextView tvWallet;
    @BindView(R.id.tvCurrency)
    CustomTextView tvCurrency;
    @BindView(R.id.layWallet)
    RelativeLayout layWallet;
    @BindView(R.id.layHeaderWallet)
    RelativeLayout layHeaderWallet;
    @BindView(R.id.ivQr)
    ImageView ivQr;
    @BindView(R.id.layTxtMemindai)
    CustomTextView layTxtMemindai;
    @BindView(R.id.etJumlah)
    CustomTextView etJumlah;
    @BindView(R.id.spinner1)
    Spinner spMeasurement;
    @BindView(R.id.btnGenerateQR)
    CustomButton btnGenerateQR;
    @BindView(R.id.layFooterButton)
    LinearLayout layFooterButton;
    @BindView(R.id.etAmount)
    CustomEditText etAmount;
    @BindView(R.id.etPrice)
    CustomEditText etPrice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_get_rice_qr, container, false);
        unbinder = ButterKnife.bind(this, v);
        tvWallet.setText(Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(getContext())));
        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnGenerateQR)
    public void onViewClicked() {

        String jsonStr = Intib._getInstance().getLocalValue(getContext(), Constant.PROFILE) != null ?
                Intib._getInstance().getLocalValue(getContext(), Constant.PROFILE).toString() : "";

        String username = "";
        if (!jsonStr.equalsIgnoreCase("")) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(jsonStr);
                username = obj.getString("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (!etAmount.getText().toString().trim().equalsIgnoreCase("") && !etAmount.getText().toString().trim().equalsIgnoreCase("0")) {
            if (!etPrice.getText().toString().trim().equalsIgnoreCase("") && !etPrice.getText().toString().trim().equalsIgnoreCase("0")) {
                String amount = etAmount.getText().toString();
                String price = etPrice.getText().toString();
                Intib._getInstance().getQRAddress(getActivity(), ivQr, 800, amount,username,price);
            }else{
                Intib._getInstance().showError(getContext(), "Harga Beras Tidak Boleh Kosong!!!");
            }

        } else {
            Intib._getInstance().showError(getContext(), "Jumlah Beras Tidak Boleh Kosong!!!");
        }
    }

    private String genErrorInvalidFormat(int fld, int msgCode) {
        String fieldName = getResources().getString(fld);
        String strInvalidFormat = getResources().getString(msgCode);
        String strInvalidFormatMsg = String.format(strInvalidFormat, fieldName);
        return strInvalidFormatMsg;
    }
}
