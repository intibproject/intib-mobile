package com.intib.activity.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.GetRiceConfirmActivity;
import com.intib.activity.ReceiptHistActivity;
import com.intib.activity.adapter.GetRiceTrxListAdapter;
import com.intib.activity.adapter.HistTrxListAdapter;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.HistoryTrxDetailInformation;
import com.intib.model.HistoryTrxInformation;
import com.intib.model.PendingTransactionDetailInformation;
import com.intib.model.PendingTransactionInformation;
import com.intib.util.CustomDateFormat;
import com.intib.util.DividerItemDecoration;
import com.intib.util.RecyclerItemClickListener;
import com.intib.util.fonts.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryTrxListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeToRefreshLayout;
    @BindView(R.id.tvTrans)
    CustomTextView tvTrans;
    Unbinder unbinder;
    private List<HistoryTrxDetailInformation> histInformations = new ArrayList<HistoryTrxDetailInformation>();

    HistTrxListAdapter histTrxListAdapter;
    ApiManager manager = Intib._getClient().create(ApiManager.class) ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_get_rice_list, container, false);

        unbinder = ButterKnife.bind(this, v);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());

        swipeToRefreshLayout.setOnRefreshListener(this);

        if (histInformations.size() > 0){
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }

        revList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), revList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Intent receiptIntent = new Intent(getActivity(), ReceiptHistActivity.class);

                        if(histInformations.get(i).getAddressKey().trim().equals(histInformations.get(i).getSender().trim())){
                            receiptIntent.putExtra("status",
                                    "KIRIM BERAS");
                        }else if(histInformations.get(i).getAddressKey().trim().equals(histInformations.get(i).getReceiver().trim())) {
                            receiptIntent.putExtra("status",
                                    "TERIMA BERAS");
                        }

                        receiptIntent.putExtra("date",
                                CustomDateFormat._getInstance().getDateTime(Long.parseLong(histInformations.get(i).getCreatedAt().toString())));
                        receiptIntent.putExtra("sender",
                                histInformations.get(i).getSenderUsername());
                        receiptIntent.putExtra("receiver",
                                histInformations.get(i).getReceiverUsername());
                        receiptIntent.putExtra("trxId",
                                String.valueOf(histInformations.get(i).getTransactionId()));
                        receiptIntent.putExtra("amount",
                                histInformations.get(i).getAmount());
                        receiptIntent.putExtra("price",
                                histInformations.get(i).getPrice());
                        receiptIntent.putExtra("total",
                                histInformations.get(i).getPrice()*histInformations.get(i).getAmount());
                        startActivity(receiptIntent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        histInformations = Intib._getInstance().getInternalDB(getContext()).getAllTransactionHist();

        if(histInformations.size() <= 0){
            new HistoryTrxListFragment.ReportTask().execute();
        }

        histTrxListAdapter = new HistTrxListAdapter(histInformations);
        revList.setAdapter(histTrxListAdapter);

        if (histInformations.size() > 0){
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new HistoryTrxListFragment.ReportTask().execute();
    }

    public class ReportTask extends AsyncTask<String, Void, String> {

        public ReportTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            histInformations.clear();
            Intib._getInstance().getInternalDB(getContext()).resetHistRice();
        }

        @Override
        protected String doInBackground(String... strings) {
            final SqlLiteDB db = new SqlLiteDB(getContext());

            /*GetRiceInformation obj = new GetRiceInformation();
            obj.setTrxId("001");
            obj.setSenderName("fadhil");
            obj.setReceiverName("rima");
            obj.setAmount(1000);
            obj.setPrice(5000);
            obj.setTotal(5000000);
            obj.setStatus("PENDING");
            obj.setDatetime("1511860272");
            riceInformations.add(obj);

            GetRiceInformation obj1 = new GetRiceInformation();
            obj1.setTrxId("002");
            obj1.setSenderName("eka");
            obj1.setReceiverName("rahma");
            obj1.setAmount(50);
            obj1.setPrice(3000);
            obj1.setTotal(150000);
            obj1.setStatus("APPROVED");
            obj1.setDatetime("1511860272");
            riceInformations.add(obj);

            db.addTrxGetRice(riceInformations);*/
            try {
                AddressInformation param = new AddressInformation();
                param.setAddressKey(Intib._getInstance().getPublicAddress(getContext()));
                Call<HistoryTrxInformation> histTransaction = manager.histTransaction(param);
                histTransaction.enqueue(new Callback<HistoryTrxInformation>() {
                    @Override
                    public void onResponse(Call<HistoryTrxInformation> call, Response<HistoryTrxInformation> response) {
                        if(response.body() != null) {
                            HistoryTrxInformation obj = response.body();
                            String addressKey = Intib._getInstance().getPublicAddress(getContext());
                            for(HistoryTrxDetailInformation objDetail : obj.getHistory()){
                                HistoryTrxDetailInformation info = new HistoryTrxDetailInformation();
                                info.setTransactionId(objDetail.getTransactionId());
                                info.setSender(objDetail.getSender());
                                info.setReceiver(objDetail.getReceiver());
                                info.setSenderUsername(objDetail.getSenderUsername());
                                info.setReceiverUsername(objDetail.getReceiverUsername());
                                info.setCreatedAt(objDetail.getCreatedAt());
                                info.setAmount(objDetail.getAmount());
                                info.setPrice(objDetail.getPrice());
                                info.setAddressKey(addressKey);
                                histInformations.add(info);
                            }
                            db.addTrxHist(histInformations);
                            histTrxListAdapter = new HistTrxListAdapter(histInformations);
                            revList.setAdapter(histTrxListAdapter);

                            if (histInformations.size() > 0){
                                tvTrans.setVisibility(View.GONE);
                            } else {
                                tvTrans.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<HistoryTrxInformation> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            swipeToRefreshLayout.setRefreshing(false);
            histTrxListAdapter = new HistTrxListAdapter(histInformations);
            revList.setAdapter(histTrxListAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
