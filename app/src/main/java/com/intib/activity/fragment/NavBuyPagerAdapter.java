package com.intib.activity.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class NavBuyPagerAdapter extends FragmentPagerAdapter {
    private List<String> mDataList;

    public NavBuyPagerAdapter(FragmentManager fm, List<String> mDataList) {
        super(fm);
        this.mDataList = mDataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new BuyTrxListFragment();
            default:
                return null;
        }

    }
}
