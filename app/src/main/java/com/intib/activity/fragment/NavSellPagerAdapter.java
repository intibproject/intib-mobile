package com.intib.activity.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/28/17.
 */

public class NavSellPagerAdapter extends FragmentPagerAdapter {
    private List<String> mDataList;

    public NavSellPagerAdapter(FragmentManager fm, List<String> mDataList) {
        super(fm);
        this.mDataList = mDataList;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new SellFragment();
            case 1:
                return new SellListFragment();
            case 2:
                return new SellApprovalFragment();
            default:
                return null;
        }

    }
}
