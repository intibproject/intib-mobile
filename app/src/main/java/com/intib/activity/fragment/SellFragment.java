package com.intib.activity.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonParseException;
import com.intib.Intib;
import com.intib.R;
import com.intib.manager.ApiManager;
import com.intib.model.SellInformation;
import com.intib.util.Constant;
import com.intib.util.button.CustomButton;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.tvWallet)
    CustomTextView tvWallet;
    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    @BindView(R.id.tvAmount)
    CustomEditText tvAmount;
    @BindView(R.id.tvPrice)
    CustomEditText tvPrice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sell, container, false);
        String stock = Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(getContext())) == null ? "" :
                Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(getContext()));
        unbinder = ButterKnife.bind(this, v);
        tvWallet.setText(stock);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        try {

            String jsonStr = Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE) != null ?
                    Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE).toString() : "";

            String myUsername = "";
            JSONObject obj = null;
            try {
                obj = new JSONObject(jsonStr);
                myUsername = obj.getString("username");
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final String addressKey = Intib._getInstance().getPublicAddress(getContext());
            final String amount = !tvAmount.getText().toString().equalsIgnoreCase("") ? tvAmount.getText().toString() : "0";
            final String price = !tvPrice.getText().toString().equalsIgnoreCase("") ? tvPrice.getText().toString() : "0";

            if (amount.equalsIgnoreCase("0")) {
                Intib._getInstance().showError(getContext(), "Jumlah Beras Harus Diisi");
            } else {
                int inAmount = Integer.parseInt(amount);
                int inStock = Integer.parseInt(Intib._getInstance().getStock(getActivity()));
                if (inAmount > inStock) {
                    Intib._getInstance().showError(getContext(), "Jumlah Beras Melebihi Stok");
                } else {
                    if (price.equalsIgnoreCase("0")) {
                        Intib._getInstance().showError(getContext(), "OpenSans-Regular.ttf Harus Diisi");
                    } else {
                        SellInformation param = new SellInformation();
                        param.setSeller(addressKey);
                        param.setAmount(Integer.parseInt(amount));
                        param.setPrice(Integer.parseInt(price));

                        Call<ResponseBody> checkAccountByUsername = manager.sellProduct(param);
                        checkAccountByUsername.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                                    if (response.code() == HttpStatus.SC_OK) {
                                        tvAmount.setText("");
                                        tvPrice.setText("");
                                        Intib._getInstance().showSuccess(getActivity(), "Data Penjualan Tersimpan");
                                    } else {
                                        Intib._getInstance().showError(getActivity(), "Gagal Menyimpan Data Penjualan");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Intib._getInstance().showError(getActivity(), "Gagal Menyimpan Data Penjualan");
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            Log.e("intib", e.getMessage());
        }
    }
}
