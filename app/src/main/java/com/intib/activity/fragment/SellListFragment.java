package com.intib.activity.fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intib.Intib;
import com.intib.R;
import com.intib.activity.ReceiptSellActivity;
import com.intib.activity.adapter.GetRiceTrxListAdapter;
import com.intib.activity.adapter.SellRiceTrxListAdapter;
import com.intib.core.db.SqlLiteDB;
import com.intib.manager.ApiManager;
import com.intib.model.AddressInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.PendingTransactionDetailInformation;
import com.intib.model.PendingTransactionInformation;
import com.intib.model.SellInformation;
import com.intib.model.SellTransactionDetailInformation;
import com.intib.model.SellTransactionInformation;
import com.intib.util.CustomDateFormat;
import com.intib.util.DividerItemDecoration;
import com.intib.util.RecyclerItemClickListener;
import com.intib.util.fonts.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.revList)
    RecyclerView revList;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeToRefreshLayout;
    @BindView(R.id.tvTrans)
    CustomTextView tvTrans;
    Unbinder unbinder;
    private List<SellInformation> sellInformations = new ArrayList<SellInformation>();

    SellRiceTrxListAdapter sellRiceTrxListAdapter;
    ApiManager manager = Intib._getClient().create(ApiManager.class);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sell_list, container, false);

        unbinder = ButterKnife.bind(this, v);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        revList.setLayoutManager(mLayoutManager);
        revList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        revList.setItemAnimator(new DefaultItemAnimator());

        swipeToRefreshLayout.setOnRefreshListener(this);

        new ReportTaskSell().execute();

        revList.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), revList, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Intent receiptIntent = new Intent(getActivity(), ReceiptSellActivity.class);
                        receiptIntent.putExtra("date",
                                CustomDateFormat._getInstance().getDateYR(Long.parseLong(sellInformations.get(i).getDatetime())));
                        receiptIntent.putExtra("time",
                                CustomDateFormat._getInstance().getDateHR(Long.parseLong(sellInformations.get(i).getDatetime())));
                        receiptIntent.putExtra("status",
                                "JUAL");
                        receiptIntent.putExtra("trxId",
                                sellInformations.get(i).getTrxId());
                        receiptIntent.putExtra("amount",
                                String.valueOf(Intib._getInstance().formatAmountWithKilo(sellInformations.get(i).getAmount())));
                        receiptIntent.putExtra("price",
                                String.valueOf(Intib._getInstance().RupiahFormat(sellInformations.get(i).getPrice())));
                        receiptIntent.putExtra("total",
                                String.valueOf(Intib._getInstance().RupiahFormat(
                                        sellInformations.get(i).getAmount()*sellInformations.get(i).getPrice())));
                        startActivity(receiptIntent);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        sellInformations = Intib._getInstance().getInternalDB(getContext()).getAllTransactionSell();

        if(sellInformations.size() <= 0){
            new SellListFragment.ReportTaskSell().execute();
        }

        sellRiceTrxListAdapter = new SellRiceTrxListAdapter(sellInformations);
        revList.setAdapter(sellRiceTrxListAdapter);

        if (sellInformations.size() > 0) {
            tvTrans.setVisibility(View.GONE);
        } else {
            tvTrans.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        new ReportTaskSell().execute();
    }

    public class ReportTaskSell extends AsyncTask<String, Void, String> {

        public ReportTaskSell() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sellInformations.clear();
            Intib._getInstance().getInternalDB(getContext()).resetSellRice();
        }

        @Override
        protected String doInBackground(String... strings) {
            final SqlLiteDB db = new SqlLiteDB(getContext());

            /*SellInformation obj = new SellInformation();
            obj.setTrxId("001");
            obj.setBuyerName("fadhil");
            obj.setAmount(1000);
            obj.setPrice(5000);
            obj.setTotal(5000000);
            obj.setStatus("JUAL");
            obj.setDatetime("1511860272");
            obj.setLabelTrx("Jual Beras");
            sellInformations.add(obj);
            db.addTrxSellRice(sellInformations);

            obj = new SellInformation();
            obj.setTrxId("002");
            obj.setBuyerName("fadhil");
            obj.setAmount(50);
            obj.setPrice(3000);
            obj.setTotal(150000);
            obj.setStatus("KONFIRMASI");
            obj.setDatetime("1511860272");
            obj.setLabelTrx("Jual Beras");
            sellInformations.add(obj);
            db.addTrxSellRice(sellInformations);*/
            try {
                AddressInformation param = new AddressInformation();
                param.setAddressKey(Intib._getInstance().getPublicAddress(getContext()));
                Call<SellTransactionInformation> getTransaction = manager.getProduct(param);
                getTransaction.enqueue(new Callback<SellTransactionInformation>() {
                    @Override
                    public void onResponse(Call<SellTransactionInformation> call, Response<SellTransactionInformation> response) {
                        if(response.body() != null) {
                            SellTransactionInformation obj = response.body();
                            for(SellTransactionDetailInformation objDetail : obj.getData()){
                                SellInformation sellInformation = new SellInformation();
                                sellInformation.setTrxId(String.valueOf(objDetail.getMarketId()));
                                sellInformation.setAmount(objDetail.getAmount());
                                sellInformation.setPrice(objDetail.getPrice());
                                sellInformation.setDatetime(String.valueOf(objDetail.getCreatedAt()));
                                sellInformation.setTotal(objDetail.getPrice()*objDetail.getAmount());
                                sellInformations.add(sellInformation);
                            }
                            db.addTrxSellRice(sellInformations);
                            sellRiceTrxListAdapter = new SellRiceTrxListAdapter(sellInformations);
                            revList.setAdapter(sellRiceTrxListAdapter);

                            if (sellInformations.size() > 0){
                                tvTrans.setVisibility(View.GONE);
                            } else {
                                tvTrans.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<SellTransactionInformation> call, Throwable t) {
                        System.out.println(t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {
            swipeToRefreshLayout.setRefreshing(false);
            sellRiceTrxListAdapter = new SellRiceTrxListAdapter(sellInformations);
            revList.setAdapter(sellRiceTrxListAdapter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}