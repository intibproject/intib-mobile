package com.intib.activity.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.JsonParseException;
import com.intib.Intib;
import com.intib.R;
import com.intib.activity.SendConfirmActivity;
import com.intib.manager.ApiManager;
import com.intib.model.UsernameInformation;
import com.intib.util.Constant;
import com.intib.util.button.CustomButton;
import com.intib.util.editText.CustomEditText;
import com.intib.util.fonts.CustomTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cz.msebera.android.httpclient.HttpStatus;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendFragment extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.btnSubmit)
    CustomButton btnSubmit;

    ApiManager manager = Intib._getClient().create(ApiManager.class);
    @BindView(R.id.spinner1)
    Spinner spMeasurement;
    @BindView(R.id.tvUsername)
    CustomEditText tvUsername;
    @BindView(R.id.tvAmount)
    CustomEditText tvAmount;
    @BindView(R.id.tvPrice)
    CustomEditText tvPrice;
    @BindView(R.id.tvWallet)
    CustomTextView tvWallet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_send, container, false);

        unbinder = ButterKnife.bind(this, v);


        List<String> measureList = new ArrayList<>();
        measureList.add("Kilogram");
        measureList.add("Ton");
        measureList.add("Gram");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.item_bank_sp, measureList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMeasurement.setAdapter(dataAdapter);
        tvWallet.setText(Intib._getInstance().formatAmountWithKilo(Intib._getInstance().getStock(getContext())));
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private String genErrorInvalidFormat(int fld, int msgCode) {
        String fieldName = getResources().getString(fld);
        String strInvalidFormat = getResources().getString(msgCode);
        String strInvalidFormatMsg = String.format(strInvalidFormat, fieldName);
        return strInvalidFormatMsg;
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        try {

            String jsonStr = Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE) != null ?
                    Intib._getInstance().getLocalValue(getActivity(), Constant.PROFILE).toString() : "";

            String myUsername = "";
            JSONObject obj = null;
            try {
                obj = new JSONObject(jsonStr);
                myUsername = obj.getString("username");
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final String username = tvUsername.getText() != null ? tvUsername.getText().toString() : "";
            final String amount = !tvAmount.getText().toString().equalsIgnoreCase("")  ? tvAmount.getText().toString() : "0";
            final String price = !tvPrice.getText().toString().equalsIgnoreCase("") ? tvPrice.getText().toString() : "0";
            final String measure = tvPrice.getText() != null ? tvPrice.getText().toString() : "0";

            if (username.equalsIgnoreCase("")) {
                Intib._getInstance().showError(getContext(), "Username Harus Diisi");
            } else {
                if (username.equalsIgnoreCase(myUsername)) {
                    Intib._getInstance().showError(getContext(), "Tidak Bisa Mengirim Ke Akun Sendiri");
                } else {
                    if (amount.equalsIgnoreCase("0")) {
                        Intib._getInstance().showError(getContext(), "Jumlah Beras Harus Diisi");
                    } else {
                        int inAmount = Integer.parseInt(amount);
                        int inStock = Integer.parseInt(Intib._getInstance().getStock(getActivity()));
                        if (inAmount > inStock) {
                            Intib._getInstance().showError(getContext(), "Jumlah Beras Melebihi Stok");
                        } else {
                            if (price.equalsIgnoreCase("0")) {
                                Intib._getInstance().showError(getContext(), "OpenSans-Regular.ttf Harus Diisi");
                            } else {
                                UsernameInformation param = new UsernameInformation();
                                param.setUsername(username);

                                Call<ResponseBody> checkAccountByUsername = manager.checkAccountByUsername(param);
                                checkAccountByUsername.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        try {
                                            if (response.code() == HttpStatus.SC_OK) {
                                                String jsonData = response.body().string();
                                                if (!jsonData.equalsIgnoreCase("")) {
                                                    JSONObject jsonObj = new JSONObject(jsonData);
                                                    String receiverAddress = jsonObj.getString("addressKey");
                                                    Intent intent = new Intent(getContext(), SendConfirmActivity.class);
                                                    intent.putExtra("title", "Kirim Beras via Username");
                                                    intent.putExtra("receiver", username);
                                                    intent.putExtra("amount", amount);
                                                    intent.putExtra("price", price);
                                                    intent.putExtra("receiverAddress", receiverAddress);
                                                    getActivity().startActivity(intent);
                                                } else {
                                                    Intib._getInstance().showError(getActivity(), "Username Tidak Terdaftar");
                                                }
                                            } else if (response.code() == HttpStatus.SC_NOT_FOUND) {
                                                Intib._getInstance().showError(getActivity(), "Username Tidak Terdaftar");
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("intib", e.getMessage());
        }
    }
}
