package com.intib.activity.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.intib.Intib;
import com.intib.R;
import com.intib.activity.SendConfirmActivity;
import com.intib.util.SweetDialog;
import com.intib.util.qrcode.ZXingScannerView;
import static android.Manifest.permission.CAMERA;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SendQRCodeFragment extends Fragment implements ZXingScannerView.ResultHandler {
    Unbinder unbinder;
    @BindView(R.id.layWallet)
    RelativeLayout layWallet;
    @BindView(R.id.layHeaderWallet)
    RelativeLayout layHeaderWallet;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    private ZXingScannerView mScannerView;

    private static final int REQUEST_CAMERA = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_send_qrcode, container, false);
        unbinder = ButterKnife.bind(this, v);

        List<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);

        ViewGroup contentFrame = (ViewGroup) v.findViewById(R.id.content_frame);
        mScannerView = new ZXingScannerView(getActivity().getApplicationContext());
        mScannerView.setLaserEnabled(false);
        mScannerView.setIsBorderCornerRounded(true);
        mScannerView.setSquareViewFinder(true);
        mScannerView.setBorderLineLength(200);
        mScannerView.setBorderCornerRadius(20);
        mScannerView.setFormats(formats);
        contentFrame.addView(mScannerView);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {

            } else {
                requestPermission();
            }
        }

        return v;
    }

    private boolean checkPermission() {
        return ( ContextCompat.checkSelfPermission(getContext(), CAMERA ) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted){

                    }else {
                        Intib._getInstance().showError(getContext(),"Permission Denied, You cannot access and camera");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(Result rawResult) {
        final Context _this = getActivity();

        String _data = new String(Intib._getInstance().hexStringToByteArray(rawResult.toString()));
        final String address;
        final String[] datas;
        final String amount;
        final String price;
        final String username;
        try {
            address = _data.split("retrive:")[1].split("/")[0];
            datas = _data.split("retrive:")[1].split("/");
            amount = datas[1];
            price = datas[2];
            username = datas[3];
        } catch (Exception exc) {
            mScannerView.startCamera();
            Intib._getInstance().showError(_this, "QRCode tidak dikenali");
            getActivity().finish();
            return;
        }
        if (_data.startsWith("retrive")) {
            int saldo = Integer.parseInt(Intib._getInstance().getStock(getContext()));
            if (Integer.parseInt(amount) <= saldo) {
                SweetDialog.ProgressDialog((Activity) _this);
                try {
                    SweetDialog.Dismiss();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                SweetDialog.Dismiss();
                                if (!address
                                        .equals(Intib._getInstance().getPublicAddress(_this).toLowerCase())) {
                                    Intent intent = new Intent(getActivity(), SendConfirmActivity.class);
                                    intent.putExtra("title", "Kirim Beras via QR Code");
                                    intent.putExtra("receiver", username);
                                    intent.putExtra("amount", amount);
                                    intent.putExtra("price", price);
                                    intent.putExtra("receiverAddress", address);
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    Intib._getInstance().showError(getContext(),"Tidak dapat mengirim ke akun sendiri");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    return;
                }

            } else {
                Intib._getInstance().showError(getContext(),"Stok Tidak Mencukupi");
            }
        }
        mScannerView.startCamera();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }
}
