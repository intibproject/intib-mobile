package com.intib.core.db;

import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.intib.util.Constant;

import java.io.IOException;

/**
 * Created by Fadhilla Eka Hentino on 11/18/17.
 */

public class CBManager {
    public static synchronized Manager _getManager(Context context){
        Manager manager = null;
        try {
            manager = new Manager(new AndroidContext(context),Manager.DEFAULT_OPTIONS);
        }catch (IOException e){
            e.printStackTrace();
        }
        return manager;
    }

    public static synchronized Database _getInstance(Context context){
        Database database = null;
        try {
            database = CBManager._getManager(context).getDatabase(Constant.DATABASE_NAME);
        }catch (CouchbaseLiteException e){
            e.printStackTrace();
        }
        return database;
    }
}
