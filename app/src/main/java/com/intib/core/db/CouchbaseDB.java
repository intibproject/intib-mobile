package com.intib.core.db;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.intib.util.Constant;


/**
 * Created by Fadhilla Eka Hentino on 11/15/17.
 */

public class CouchbaseDB {

    private static CouchbaseDB _instance;

    public static CouchbaseDB _getInstance(){
        if(_instance == null){
            _instance = new CouchbaseDB();
        }
        return _instance;
    }

    public Database getDB(android.content.Context context){
        Manager manager = null;
        try {
            manager = new Manager(new AndroidContext(context.getApplicationContext()), manager.DEFAULT_OPTIONS);
        }catch (Exception e){
            e.printStackTrace();
        }
        Database database = null;
        try {
            database = manager.getDatabase(Constant.DATABASE_NAME);
        } catch (CouchbaseLiteException e) {
            return null;
        }
        return database;
    }
}
