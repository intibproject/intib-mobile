package com.intib.core.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.intib.model.BuyNotifInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.HistoryTrxDetailInformation;
import com.intib.model.SellInformation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/28/17.
 */

public class SqlLiteDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "digiroin.db";
    private static final String TABLE_TRANSACTION_GET_RICE = "trxs";
    private static final String TABLE_TRANSACTION_SELL_RICE = "trxSell";
    private static final String TABLE_TRANSACTION_BUY_RICE = "trxBuy";
    private static final String TABLE_TRANSACTION_SELL_RICE_APV = "trxSellApproval";
    private static final String TABLE_TRANSACTION_HIST = "trxHist";

    private static final String KEY_trxId = "trxId";
    private static final String KEY_senderName = "senderName";
    private static final String KEY_receiverName = "receiverName";
    private static final String KEY_sender = "sender";
    private static final String KEY_receiver = "receiver";
    private static final String KEY_datetime = "datetime";
    private static final String KEY_amount = "amount";
    private static final String KEY_price = "price";
    private static final String KEY_total = "total";
    private static final String KEY_status= "status";
    private static final String KEY_addressKey= "addressKey";

    private static final String KEY_requestId ="requestId" ;
    private static final String KEY_marketId = "marketId" ;
    private static final String KEY_buyer = "buyer";
    private static final String KEY_quantity = "quantity";
    private static final String KEY_isprocessed = "isProcessed";
    private static final String KEY_username = "username" ;
    private static final String KEY_buyerName= "buyerName";

    private static SqlLiteDB sqlLiteDB = null;

    public static SqlLiteDB _getInstance(Context context){
        if(sqlLiteDB == null){
            sqlLiteDB = new SqlLiteDB(context);
        }
        return sqlLiteDB;
    }

    public SqlLiteDB(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    private String trxId = "";
    private String buyerName = "";
    private String datetime = "";
    private String status = "";
    private Integer amount = 0;
    private Integer price = 0;
    private Integer total = 0;

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TRANSACTION_TABLE = "CREATE TABLE " + TABLE_TRANSACTION_GET_RICE + "("
                + KEY_trxId + " TEXT PRIMARY KEY," + KEY_senderName + " TEXT,"
                + KEY_receiverName + " TEXT," + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT," + KEY_total + " TEXT," + KEY_status + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE);

        String CREATE_TRANSACTION_TABLE_SELL = "CREATE TABLE " + TABLE_TRANSACTION_SELL_RICE + "("
                + KEY_trxId + " TEXT PRIMARY KEY,"
                + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT," + KEY_total + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_SELL);

        String CREATE_TRANSACTION_TABLE_HIST = "CREATE TABLE " + TABLE_TRANSACTION_HIST+ "("
                + KEY_trxId + " TEXT PRIMARY KEY,"
                + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT" +
                "," + KEY_senderName + " TEXT" +
                "," + KEY_receiverName + " TEXT" +
                "," + KEY_sender + " TEXT" +
                "," + KEY_receiver + " TEXT" +
                "," + KEY_addressKey + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_HIST);

        String CREATE_TRANSACTION_TABLE_BUY = "CREATE TABLE " + TABLE_TRANSACTION_BUY_RICE+ "("
                + KEY_requestId + " TEXT PRIMARY KEY,"
                + KEY_marketId + " TEXT,"
                + KEY_buyer + " TEXT," + KEY_quantity + " TEXT" +
                "," + KEY_isprocessed + " TEXT" +
                "," + KEY_username + " TEXT" +
                "," + KEY_price + " TEXT" +
                "," + KEY_amount + " TEXT" +
                "," + KEY_datetime + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_BUY);

        String CREATE_TRANSACTION_TABLE_SELL_APV = "CREATE TABLE " + TABLE_TRANSACTION_SELL_RICE_APV+ "("
                + KEY_requestId + " TEXT PRIMARY KEY,"
                + KEY_marketId + " TEXT,"
                + KEY_buyer + " TEXT," + KEY_quantity + " TEXT" +
                "," + KEY_isprocessed + " TEXT" +
                "," + KEY_username + " TEXT" +
                "," + KEY_price + " TEXT" +
                "," + KEY_amount + " TEXT" +
                "," + KEY_datetime + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_SELL_APV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String CREATE_TRANSACTION_TABLE = "CREATE TABLE " + TABLE_TRANSACTION_GET_RICE + "("
                + KEY_trxId + " TEXT PRIMARY KEY," + KEY_senderName + " TEXT,"
                + KEY_receiverName + " TEXT," + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT," + KEY_total + " TEXT," + KEY_status + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE);

        String CREATE_TRANSACTION_TABLE_SELL = "CREATE TABLE " + TABLE_TRANSACTION_SELL_RICE + "("
                + KEY_trxId + " TEXT PRIMARY KEY," + KEY_buyerName + " TEXT,"
                + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT," + KEY_total + " TEXT," + KEY_status + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_SELL);

        String CREATE_TRANSACTION_TABLE_HIST = "CREATE TABLE " + TABLE_TRANSACTION_HIST+ "("
                + KEY_trxId + " TEXT PRIMARY KEY,"
                + KEY_datetime + " TEXT,"
                + KEY_amount + " TEXT," + KEY_price + " TEXT" +
                "," + KEY_senderName + " TEXT" +
                "," + KEY_receiverName + " TEXT" +
                "," + KEY_sender + " TEXT" +
                "," + KEY_receiver + " TEXT" +
                "," + KEY_addressKey + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_HIST);

        String CREATE_TRANSACTION_TABLE_BUY = "CREATE TABLE " + TABLE_TRANSACTION_BUY_RICE+ "("
                + KEY_requestId + " TEXT PRIMARY KEY,"
                + KEY_marketId + " TEXT,"
                + KEY_buyer + " TEXT," + KEY_quantity + " TEXT" +
                "," + KEY_isprocessed + " TEXT" +
                "," + KEY_username + " TEXT" +
                "," + KEY_price + " TEXT" +
                "," + KEY_amount + " TEXT" +
                "," + KEY_datetime + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_BUY);

        String CREATE_TRANSACTION_TABLE_SELL_APV = "CREATE TABLE " + TABLE_TRANSACTION_SELL_RICE_APV+ "("
                + KEY_requestId + " TEXT PRIMARY KEY,"
                + KEY_marketId + " TEXT,"
                + KEY_buyer + " TEXT," + KEY_quantity + " TEXT" +
                "," + KEY_isprocessed + " TEXT" +
                "," + KEY_username + " TEXT" +
                "," + KEY_price + " TEXT" +
                "," + KEY_amount + " TEXT" +
                "," + KEY_datetime + " TEXT)";
        db.execSQL(CREATE_TRANSACTION_TABLE_SELL_APV);
    }

    public void resetGetRice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TRANSACTION_GET_RICE+"");
    }

    public void resetSellRice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TRANSACTION_SELL_RICE+"");
    }

    public void resetHistRice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TRANSACTION_HIST+"");
    }

    public void resetBuyRice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TRANSACTION_BUY_RICE+"");
    }

    public void resetSellAPv() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TRANSACTION_SELL_RICE_APV+"");
    }

    public boolean addTrxGetRice(List<GetRiceInformation> listData) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<ContentValues> listValues = new ArrayList<ContentValues>() ;
        try {
            for(GetRiceInformation object : listData){
                ContentValues values = new ContentValues();
                values.put(KEY_trxId, object.getTrxId());
                values.put(KEY_senderName, object.getSenderName());
                values.put(KEY_receiverName, object.getReceiverName());
                values.put(KEY_datetime, object.getDatetime());
                values.put(KEY_amount, object.getAmount());
                values.put(KEY_price, object.getPrice());
                values.put(KEY_total, object.getTotal());
                values.put(KEY_status, object.getStatus());
                listValues.add(values);
            }
        } catch (Exception exc) {
            Log.e("INTIB", exc.toString());
            return false;
        } finally {
            try {
                for(ContentValues values : listValues){
                    db.insert(""+TABLE_TRANSACTION_GET_RICE+"", null, values);
                }
                return true;
            } catch (RuntimeException rexc) {
                return false;
            }
        }
    }

    public boolean addTrxSellRice(List<SellInformation> listData) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<ContentValues> listValues = new ArrayList<ContentValues>() ;
        try {
            for(SellInformation object : listData){
                ContentValues values = new ContentValues();
                values.put(KEY_trxId, object.getTrxId());
                values.put(KEY_datetime, object.getDatetime());
                values.put(KEY_amount, object.getAmount());
                values.put(KEY_price, object.getPrice());
                values.put(KEY_total, object.getTotal());
                listValues.add(values);
            }
        } catch (Exception exc) {
            Log.e("INTIB", exc.toString());
            return false;
        } finally {
            try {
                for(ContentValues values : listValues) {
                    db.insert("" + TABLE_TRANSACTION_SELL_RICE + "", null, values);
                }
                return true;
            } catch (RuntimeException rexc) {
                return false;
            }
        }
    }

    public boolean addTrxHist(List<HistoryTrxDetailInformation> listData) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<ContentValues> listValues = new ArrayList<ContentValues>() ;
        try {
            for(HistoryTrxDetailInformation object : listData){
                ContentValues values = new ContentValues();
                values.put(KEY_trxId, String.valueOf(object.getTransactionId()));
                values.put(KEY_datetime, object.getCreatedAt());
                values.put(KEY_amount, object.getAmount());
                values.put(KEY_price, object.getPrice());
                values.put(KEY_sender, object.getSender());
                values.put(KEY_senderName, object.getSenderUsername());
                values.put(KEY_receiver, object.getReceiver());
                values.put(KEY_receiverName, object.getReceiverUsername());
                values.put(KEY_addressKey, object.getAddressKey());
                listValues.add(values);
            }
        } catch (Exception exc) {
            Log.e("INTIB", exc.toString());
            return false;
        } finally {
            try {
                for(ContentValues values : listValues) {
                    db.insert("" + TABLE_TRANSACTION_HIST + "", null, values);
                }
                return true;
            } catch (RuntimeException rexc) {
                return false;
            }
        }
    }

    public boolean addTrxBuy(List<BuyNotifInformation> listData) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<ContentValues> listValues = new ArrayList<ContentValues>() ;
        try {
            for(BuyNotifInformation object : listData){
                ContentValues values = new ContentValues();
                values.put(KEY_requestId, String.valueOf(object.getRequestId()));
                values.put(KEY_marketId, String.valueOf(object.getMarketplaceId()));
                values.put(KEY_buyer, object.getBuyer());
                values.put(KEY_isprocessed, object.isProcessed() == true? "1" : "0" );
                values.put(KEY_username, object.getUsername());
                values.put(KEY_price, object.getPrice());
                values.put(KEY_amount, object.getAmount());
                values.put(KEY_datetime, object.getCreatedAt());
                listValues.add(values);
            }
        } catch (Exception exc) {
            Log.e("INTIB", exc.toString());
            return false;
        } finally {
            try {
                for(ContentValues values : listValues) {
                    db.insert("" + TABLE_TRANSACTION_BUY_RICE + "", null, values);
                }
                return true;
            } catch (RuntimeException rexc) {
                return false;
            }
        }
    }

    public boolean addTrxSellApv(List<BuyNotifInformation> listData) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<ContentValues> listValues = new ArrayList<ContentValues>() ;
        try {
            for(BuyNotifInformation object : listData){
                ContentValues values = new ContentValues();
                values.put(KEY_requestId, String.valueOf(object.getRequestId()));
                values.put(KEY_marketId, String.valueOf(object.getMarketplaceId()));
                values.put(KEY_buyer, object.getBuyer());
                values.put(KEY_isprocessed, object.isProcessed() == true? "1" : "0" );
                values.put(KEY_username, object.getUsername());
                values.put(KEY_price, object.getPrice());
                values.put(KEY_amount, object.getAmount());
                values.put(KEY_datetime, object.getCreatedAt());
                listValues.add(values);
            }
        } catch (Exception exc) {
            Log.e("INTIB", exc.toString());
            return false;
        } finally {
            try {
                for(ContentValues values : listValues) {
                    db.insert("" + TABLE_TRANSACTION_SELL_RICE_APV + "", null, values);
                }
                return true;
            } catch (RuntimeException rexc) {
                return false;
            }
        }
    }

    public List<GetRiceInformation> getAllTransactionGetRice() {
        List<GetRiceInformation> contactList = new ArrayList<GetRiceInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_GET_RICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    GetRiceInformation riceInformation = new GetRiceInformation();
                    riceInformation.setTrxId(cursor.getString(0));
                    riceInformation.setSenderName(cursor.getString(1));
                    riceInformation.setReceiverName(cursor.getString(2));
                    riceInformation.setDatetime(cursor.getString(3));
                    riceInformation.setAmount(Integer.parseInt(cursor.getString(4)));
                    riceInformation.setPrice(Integer.parseInt(cursor.getString(5)));
                    riceInformation.setTotal(Integer.parseInt(cursor.getString(6)));
                    riceInformation.setStatus(cursor.getString(7));
                    contactList.add(riceInformation);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return contactList;
    }
    public List<SellInformation> getAllTransactionSell() {
        List<SellInformation> sellList = new ArrayList<SellInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_SELL_RICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    SellInformation obj = new SellInformation();
                    obj.setTrxId(cursor.getString(0));
                    obj.setDatetime(cursor.getString(1));
                    obj.setAmount(Integer.parseInt(cursor.getString(2)));
                    obj.setPrice(Integer.parseInt(cursor.getString(3)));
                    obj.setTotal(Integer.parseInt(cursor.getString(4)));
                    sellList.add(obj);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return sellList;
    }

    public List<BuyNotifInformation> getAllTransactionBuy() {
        List<BuyNotifInformation> sellList = new ArrayList<BuyNotifInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_BUY_RICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    BuyNotifInformation obj = new BuyNotifInformation();
                    obj.setRequestId(new BigInteger(cursor.getString(0)));
                    obj.setMarketplaceId(new BigInteger(cursor.getString(1)));
                    obj.setBuyer(cursor.getString(2));
                    obj.setProcessed(cursor.getString(3) == "1" ? true : false );
                    obj.setUsername(cursor.getString(4));
                    obj.setPrice(Integer.parseInt(cursor.getString(5)));
                    obj.setAmount(Integer.parseInt(cursor.getString(6)));
                    obj.setCreatedAt( Integer.parseInt(cursor.getString(7)));
                    sellList.add(obj);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return sellList;
    }

    public List<BuyNotifInformation> getAllTransactionSellApv() {
        List<BuyNotifInformation> sellList = new ArrayList<BuyNotifInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_SELL_RICE_APV;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    BuyNotifInformation obj = new BuyNotifInformation();
                    obj.setRequestId(new BigInteger(cursor.getString(0)));
                    obj.setMarketplaceId(new BigInteger(cursor.getString(1)));
                    obj.setBuyer(cursor.getString(2));
                    obj.setProcessed(cursor.getString(3) == "1" ? true : false );
                    obj.setUsername(cursor.getString(4));
                    obj.setPrice(Integer.parseInt(cursor.getString(5)));
                    obj.setAmount(Integer.parseInt(cursor.getString(6)));
                    obj.setCreatedAt( Integer.parseInt(cursor.getString(7)));
                    sellList.add(obj);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return sellList;
    }

    public List<HistoryTrxDetailInformation> getAllTransactionHist() {
        List<HistoryTrxDetailInformation> sellList = new ArrayList<HistoryTrxDetailInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_HIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    HistoryTrxDetailInformation obj = new HistoryTrxDetailInformation();
                    obj.setTransactionId(new BigInteger(cursor.getString(0)));
                    obj.setCreatedAt(Integer.parseInt(cursor.getString(1)));
                    obj.setAmount(Integer.parseInt(cursor.getString(2)));
                    obj.setPrice(Integer.parseInt(cursor.getString(3)));
                    obj.setSenderUsername(cursor.getString(4));
                    obj.setReceiverUsername(cursor.getString(5));
                    obj.setSender(cursor.getString(6));
                    obj.setReceiver(cursor.getString(7));
                    obj.setAddressKey(cursor.getString(8));
                    sellList.add(obj);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return sellList;
    }


    /*public List<SellInformation> getAllTransactionBuy() {
        List<SellInformation> sellList = new ArrayList<SellInformation>();
        String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTION_GET_RICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        try{
            if (cursor.moveToFirst()) {
                do {
                    SellInformation obj = new SellInformation();
                    obj.setTrxId(cursor.getString(0));
                    obj.setBuyerName(cursor.getString(1));
                    obj.setDatetime(cursor.getString(2));
                    obj.setAmount(Integer.parseInt(cursor.getString(3)));
                    obj.setPrice(Integer.parseInt(cursor.getString(4)));
                    obj.setTotal(Integer.parseInt(cursor.getString(5)));
                    obj.setStatus(cursor.getString(6));
                    sellList.add(obj);
                } while (cursor.moveToNext());
            }
        }catch (Exception exc){

        }
        return sellList;
    }*/
}
