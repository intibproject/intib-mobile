package com.intib.manager;

import com.intib.model.AddressInformation;
import com.intib.model.BuyNotifInformation;
import com.intib.model.BuyNotifParentInformation;
import com.intib.model.BuyParamInformation;
import com.intib.model.GetRiceInformation;
import com.intib.model.HistoryTrxInformation;
import com.intib.model.PendingTransactionInformation;
import com.intib.model.PointInformation;
import com.intib.model.RemoveSellInformation;
import com.intib.model.SellInformation;
import com.intib.model.SellTransactionInformation;
import com.intib.model.SendInformation;
import com.intib.model.StockInformation;
import com.intib.model.TrxIdInformation;
import com.intib.model.UserInformation;
import com.intib.model.UsernameInformation;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Fadhilla Eka Hentino on 11/14/17.
 */

public interface ApiManager {
    @GET("user_information")
    Call<List<UserInformation>> getUserInformation();

    @GET("list_get_rice")
    Call<List<GetRiceInformation>> getListGetRice();

    @Headers("Content-Type: application/json")
    @POST("balance")
    Call<ResponseBody> getLatestStock(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("generateAccount")
    Call<ResponseBody> setAddressKey(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("addAssets")
    Call<ResponseBody> setStock(@Body StockInformation body);

    @Headers("Content-Type: application/json")
    @POST("checkAccountByUsername")
    Call<ResponseBody> checkAccountByUsername(@Body UsernameInformation body);

    @Headers("Content-Type: application/json")
    @POST("createTransaction")
    Call<ResponseBody> createTransaction(@Body SendInformation body);

    @Headers("Content-Type: application/json")
    @POST("pendingTransaction")
    Call<PendingTransactionInformation> pendingTransaction(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("approveTransaction")
    Call<ResponseBody> approvedTransaction(@Body TrxIdInformation body);

    @Headers("Content-Type: application/json")
    @POST("rejectTransaction")
    Call<ResponseBody> rejectTransaction(@Body TrxIdInformation body);

    @GET("json")
    Call<ResponseBody> getLoc(@Query("latlng") String latlng);

    @Headers("Content-Type: application/json")
    @POST("updateProfile")
    Call<ResponseBody> updateProfile(@Body UserInformation body);

    @Headers("Content-Type: application/json")
    @POST("marketplace/getProductAmount")
    Call<ResponseBody> getProductAmount(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("marketplace/addProduct")
    Call<ResponseBody> sellProduct(@Body SellInformation body);

    @Headers("Content-Type: application/json")
    @POST("marketplace/removeProduct")
    Call<ResponseBody> removeProduct(@Body RemoveSellInformation body);

    @Headers("Content-Type: application/json")
    @POST("marketplace/searchProduct")
    Call<SellTransactionInformation> getProduct(@Body AddressInformation body);


    @Headers("Content-Type: application/json")
    @POST("generateTokenWeb")
    Call<ResponseBody> generateTokenWeb(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("getPoint")
    Call<ResponseBody> getPoint(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("addPoint")
    Call<ResponseBody> addPoint(@Body PointInformation body);

    @Headers("Content-Type: application/json")
    @POST("historyTransaction")
    Call<HistoryTrxInformation> histTransaction(@Body AddressInformation body);

    @Headers("Content-Type: application/json")
    @POST("marketplace/listRequestMarketplace")
    Call<BuyNotifParentInformation> buyNotifList(@Body BuyParamInformation body);

    @Headers("Content-Type: application/json")
    @POST("getInformationProfileByAddress")
    Call<UserInformation> getInformationProfileByAddress(@Body AddressInformation body);
}
