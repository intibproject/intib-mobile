package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class AddressInformation {
    private String addressKey = "";

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }
}
