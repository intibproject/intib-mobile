package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class BuyInformation {
    private String trxId = "";
    private String sellerName = "";
    private String datetime = "";
    private String status = "";
    private String labelTrx = "";
    private Integer amount = 0;
    private Integer price = 0;
    private Integer total = 0;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLabelTrx() {
        return labelTrx;
    }

    public void setLabelTrx(String labelTrx) {
        this.labelTrx = labelTrx;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
