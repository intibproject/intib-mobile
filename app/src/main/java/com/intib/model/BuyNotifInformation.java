package com.intib.model;

import java.math.BigInteger;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class BuyNotifInformation {
    private BigInteger requestId ;
    private BigInteger marketplaceId ;
    private String buyer;
    private Integer quantity;
    private boolean isProcessed;
    private String address_key ;
    private Integer price;
    private Integer amount;
    private Integer createdAt;
    private String username ;
    private String phone ;

    public BigInteger getRequestId() {
        return requestId;
    }

    public void setRequestId(BigInteger requestId) {
        this.requestId = requestId;
    }

    public BigInteger getMarketplaceId() {
        return marketplaceId;
    }

    public void setMarketplaceId(BigInteger marketplaceId) {
        this.marketplaceId = marketplaceId;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }

    public String getAddress_key() {
        return address_key;
    }

    public void setAddress_key(String address_key) {
        this.address_key = address_key;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
