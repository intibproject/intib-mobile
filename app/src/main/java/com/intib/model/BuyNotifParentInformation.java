package com.intib.model;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class BuyNotifParentInformation {
    private String message;
    private List<BuyNotifInformation> info;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BuyNotifInformation> getInfo() {
        return info;
    }

    public void setInfo(List<BuyNotifInformation> info) {
        this.info = info;
    }
}
