package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class BuyParamInformation {
    private String Address ;
    private String Buyer;
    private boolean Processed;
    private boolean ProcessedFilter;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBuyer() {
        return Buyer;
    }

    public void setBuyer(String buyer) {
        Buyer = buyer;
    }

    public boolean isProcessed() {
        return Processed;
    }

    public void setProcessed(boolean processed) {
        Processed = processed;
    }

    public boolean isProcessedFilter() {
        return ProcessedFilter;
    }

    public void setProcessedFilter(boolean processedFilter) {
        ProcessedFilter = processedFilter;
    }
}
