package com.intib.model;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class HistoryTrxInformation {
    private String message;
    private List<HistoryTrxDetailInformation> history = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HistoryTrxDetailInformation> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryTrxDetailInformation> history) {
        this.history = history;
    }
}
