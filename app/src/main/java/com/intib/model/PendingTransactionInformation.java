package com.intib.model;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class PendingTransactionInformation {
    private String message;
    private List<PendingTransactionDetailInformation> history = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PendingTransactionDetailInformation> getHistory() {
        return history;
    }

    public void setHistory(List<PendingTransactionDetailInformation> history) {
        this.history = history;
    }

}
