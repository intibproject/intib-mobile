package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class PointInformation {
    private String addressKey = "";
    private Integer point = 0;

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }
}
