package com.intib.model;

import java.math.BigInteger;

/**
 * Created by Fadhilla Eka Hentino on 12/13/17.
 */

public class RemoveSellInformation {
    private BigInteger marketId;

    public BigInteger getMarketId() {
        return marketId;
    }

    public void setMarketId(BigInteger marketId) {
        this.marketId = marketId;
    }
}
