package com.intib.model;

import java.math.BigInteger;

/**
 * Created by Fadhilla Eka Hentino on 12/3/17.
 */

public class SellTransactionDetailInformation {
    private BigInteger marketId;
    private Integer amount;
    private Integer price;
    private String seller;
    private boolean isActive;
    private Integer createdAt;

    public BigInteger getMarketId() {
        return marketId;
    }

    public void setMarketId(BigInteger marketId) {
        this.marketId = marketId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
