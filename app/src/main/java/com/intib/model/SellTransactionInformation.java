package com.intib.model;

import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 12/3/17.
 */

public class SellTransactionInformation {
    private String message;
    private List<SellTransactionDetailInformation> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SellTransactionDetailInformation> getData() {
        return data;
    }

    public void setData(List<SellTransactionDetailInformation> data) {
        this.data = data;
    }
}
