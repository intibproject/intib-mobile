package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class StockInformation {
    private String addressKey = "";
    private int amount = 0;
    private int price = 0;

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
