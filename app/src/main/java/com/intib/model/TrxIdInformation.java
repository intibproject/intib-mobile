package com.intib.model;

import com.couchbase.lite.internal.InterfaceAudience;

import java.math.BigInteger;

/**
 * Created by Fadhilla Eka Hentino on 12/2/17.
 */

public class TrxIdInformation {
    private BigInteger transactionId = BigInteger.ZERO;

    public BigInteger getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(BigInteger transactionId) {
        this.transactionId = transactionId;
    }
}
