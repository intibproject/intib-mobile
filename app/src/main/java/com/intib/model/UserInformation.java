package com.intib.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fadhilla Eka Hentino on 11/14/17.
 */

public class UserInformation {

    private
    String addressKey;
    private
    String name;
    private
    String address;
    private
    String longlat;
    private String username;
    private String phone;
    private Double lon;
    private Double lat;
    private String provinsi;
    private String kabupaten;

    @SerializedName("result")
    List<UserInformation> listUserInformation;

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLonglat() {
        return longlat;
    }

    public void setLonglat(String longlat) {
        this.longlat = longlat;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public List<UserInformation> getListUserInformation(){
        if(listUserInformation == null){
            listUserInformation = new ArrayList<UserInformation>();
        }
        return listUserInformation;
    }

    public void setListUserInformation(List<UserInformation> listUserInformation){
        this.listUserInformation = listUserInformation;
    }
}
