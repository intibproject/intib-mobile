package com.intib.model;

/**
 * Created by Fadhilla Eka Hentino on 12/1/17.
 */

public class UsernameInformation {
    private String username = "";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
