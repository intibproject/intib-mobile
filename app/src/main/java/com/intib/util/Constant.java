package com.intib.util;

/**
 * Created by Fadhilla Eka Hentino on 11/15/17.
 */

public class Constant {
    public static final String BASE_URL = "http://49952a8d.ngrok.io/api/intib/";
    public static final String BASE_URL_GMAPS = "http://maps.googleapis.com/maps/api/geocode/";

    public static final String DATABASE_NAME = "intibdb";

    public static final String PRIVATE_KEY = "private";
    public static final String PUBLIC_KEY = "public";
    public static final String PIN = "pin";

    public static final Integer SET_PIN = 0;
    public static final Integer LOGIN_PIN = 1;
    public static final Integer LOGIN_PVKey = 2;

    public static final String EXTRA_TYPE = "type";
    public static final String STOCK = "stock";
    public static final String LOCATION = "location";
    public static final String PROFILE = "profile";
}
