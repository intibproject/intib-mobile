package com.intib.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Fadhilla Eka Hentino on 11/28/17.
 */

public class CustomDateFormat {
    private static CustomDateFormat customDateFormat = null;

    public static CustomDateFormat _getInstance(){
        if(customDateFormat == null){
            customDateFormat = new CustomDateFormat();
        }
        return customDateFormat;
    }

    public String getDateYR(long timeStamp) {
        try {
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            format.setTimeZone(tz);
            String localTime = format.format(new Date(timeStamp * 1000));
            return (localTime);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public String getDateHR(long timeStamp) {
        try {
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            format.setTimeZone(tz);
            String localTime = format.format(new Date(timeStamp * 1000));
            return (localTime);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public String getDateTime(long timeStamp) {
        try {
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            format.setTimeZone(tz);
            String localTime = format.format(new Date(timeStamp * 1000));
            return (localTime);
        } catch (Exception ex) {
            return "xx";
        }
    }
}
