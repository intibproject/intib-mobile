package com.intib.util;

import org.json.JSONObject;

/**
 * Created by corechain on 7/21/17.
 */

public interface ServiceCallResult {
    void onSuccess(JSONObject object);

    void onFailed(int statuscode);
    void onTimeout();
}
