package com.intib.util;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.intib.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by dpapayas on 8/25/17.
 */

public class SweetDialog {

    static SweetAlertDialog pDialog;

    public static void ProgressDialog(Activity activity) {
        pDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void WarningDialog(Activity activity, String title, String content, String confirm) {
        pDialog = new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText(title);
        pDialog.setContentText(content);
        pDialog.setConfirmText(confirm);
        pDialog.show();
    }

    public static void Dismiss() {
        if(pDialog!=null)
            if(pDialog.isShowing())
                pDialog.dismiss();
    }

    public static void ConfirmDialogMSD(final Activity activity, String title, String content, final ServiceCallResult result){
        new MaterialStyledDialog.Builder(activity)
                .setTitle(title)
                .autoDismiss(false)
                .setIcon(R.mipmap.ic_launcher)
                .setHeaderColor(R.color.colorPrimary)
                .setDescription(content).setCancelable(false)
                .setPositiveText("Ya")
                .setNegativeText("Tidak")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {

                        if(result!=null){
                            result.onSuccess(null);
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                        if(result!=null)
                           result.onFailed(0);
                    }
                })
                .show();
    }



}
