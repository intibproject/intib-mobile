package com.intib.util.editText;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.thebrownarrow.customfont.FontCache;

/**
 * Created by Fadhilla Eka Hentino on 11/27/17.
 */

public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.applyCustomTypeface(context, attributeSet);
    }

    public CustomEditText(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.applyCustomTypeface(context, attributeSet);
    }

    private void applyCustomTypeface(Context context, AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, com.thebrownarrow.customfont.R.styleable.CustomTypeface);
        String customFont = typedArray.getString(com.thebrownarrow.customfont.R.styleable.CustomTypeface_custom_typeface);
        this.applyCustomTypeface(context, customFont);
        typedArray.recycle();
    }

    public boolean applyCustomTypeface(Context context, String asset) {
        this.setTypeface(FontCache.getTypeface(context, asset));
        return true;
    }

    public void setText(String text){
        super.setText(text);
    }
}
